# Restriccions d'ús

Per a garantir un ús més controlat i equitatiu dels nostres recursos s'han aplicat unes quotes que afecten a totes les categories. Això significa que cada categoria/centre tindrà una assignació de recursos establerta, amb l'objectiu d'evitar la saturació i assegurar que tots els usuaris tinguin accés a la plataforma de manera adequada.

Entenem que aquesta mesura pot generar algunes preguntes o inquietuds, per la qual cosa som aquí per a ajudar i brindar assistència. Si tens preguntes sobre com funcionaran les quotes o si tens alguna preocupació específica, no dubtis a contactar amb nosaltres.

## Quotes aplicades

De moment aplicarem aquestes quotes generals a totes les categories:

|Quota de creació| Quantitat de recursos que l'usuari podrà crear|
| -----------|:-----------: |
|Desktops (Quantitat d'escriptoris que l'usuari pot crear.)| 10|
|Templates (Quantitat de plantilles que l'usuari pot crear.)| 20|
|Media (Quantitat de mitjans que l'usuari pot pujar o descarregar.)| 5|

!!! Info "Info"
    **Això és individual:** Cada usuari podrà crear 10 escriptoris, 20 plantilles i pujar 5 mitjans (isos)

|Quota d'escriptoris en execució individual| Quantitat de recursos que l'usuari podrà executar alhora|
| -----------|:-----------: |
|Concurrent (Quantitat total d'escriptoris que es poden iniciar alhora.)| 4|
|vCPUs (Quantitat total de vCPUS dels escriptoris iniciats que es poden utilitzar alhora.)| 8|
|RAM (Quantitat total de memòria vRAM dels escriptoris iniciats que es poden utilitzar alhora.)| 16|

!!! Info "Info"
    **Això és individual:** Cada usuari podrà tenir 4 escriptoris arrencats, 8 de CPU i 16 GB de RAM alhora.

|Quota de mida| Mida de recursos que l'usuari pot tenir|
| -----------|:-----------: |
|Disk Size (Mesura màxima permesa en crear un escriptori des d'un mitjà.)| 100|
|Total (Mesura total que es pot utilitzar considerant escriptoris, plantilles i mitjans.)| 999|
|Soft Size (Una vegada ocupada aquesta quantitat, s'avisarà a l'usuari.)| 900|

!!! Info "Info"
    **Això és individual:** Cada usuari podrà tenir aquestes mides de recursos.

## Informació

!!! Warning "Aclariments importants"
    Els escriptoris creats a través de desplegaments no es consideren en la quota d'usuari.

    Els recursos compartits amb l'usuari no es consideren en la quota d'usuari.

    **Quan s'engega un escriptori des d'un desplegament, es considera la quota de l'usuari. Per tant, si l'usuari no té suficient quota per a engegar l'escriptori, es mostrarà un error, fins i tot si el propietari del desplegament té suficient quota. La lògica darrere d'aquest comportament és la propietat de l'escriptori, el propietari de l'escriptori és l'usuari, a pesar que va ser creat pel propietari del desplegament.**

!!! Important "Per a més informació"
    Per a més informació sobre les quotes o límits, prémer [aquí](https://isard.gitlab.io/isardvdi-docs/manager/quotas_limits.ca/)

