# Pràctica de redimensionament, creació i muntatge de particions

![Imatge GParted](gestio_particions_assets/screenshot.png){style="height:28%;width:28%" align=left} 

L'objectiu d'aquesta pràctica és disposar d'una nova partició en el disc local a partir d'encongir prèviament la partició on es trobi el sistema actualment instal·lat. D'aquesta manera, aconseguirem que les dades (si s'emmagatzemen a partir de llavors en aquesta nova partició) romanguin separades del sistema pròpiament dit, fet que facilita molt l'accés lògic a la informació guardada i la seva eventual recuperació en cas que el sistema falli per alguna raó i/o calgui reinstal·lar-lo. 


## Cicles, Mòduls i UFs on es pot realitzar aquesta pràctica

* **SMX  MP01** UF6:Instal·lació de programari
* **SMX  MP02** UF3:Sistemes operatius lliures
* **SMX  MP06** UF2:Còpies de seguretat
* **ASIX MP01** UF1:Instal·lació, configuració i explotació del sistema informàtic
* **ASIX MP06** UF1:Administració avançada de sistemes operatius
* **DAM  MP01** UF1:Instal·lació, configuració i explotació del sistema informàtic
* **DAW  MP01** UF1:Instal·lació, configuració i explotació del sistema informàtic


## Infraestructura prèvia necessària per implementar aquesta pràctica

!!! warning "Vigila l'ús de vCPUs i de RAM!"

    És **molt important** que la quantitat de vCPUs i de memòria RAM assignades a les màquines utilitzades en aquesta pràctica sigui el més adjustada possible!!
    
    La quantitat global d'hores que un centre educatiu té disponible per utilitzar IsardVDI (les anomenades "DHN", de "Desktop Hours Number") ve donada per la següent [fòrmula](https://isard.gitlab.io/isardvdi-docs-pilot-fp/usage/usage.ca/#__tabbed_1_1). Això implica que quanta més vCPUs i -sobre tot- RAM s'assignin a les màquines creades dins d'un centre concret, menys hores d'Isard aquest centre podrà consumir en total.
      
!!! info "Idees per no consumir gaire RAM"

    * Utilitzar distribucions basades en Linux de tipus "Server" (és a dir, sense entorn gràfic). Exemples de distribucions d'aquest tipus són, per exemple, [Debian Server](https://www.debian.org), [Ubuntu Server](https://ubuntu.com/download/server), [Fedora Server](https://fedoraproject.org/server), [openSUSE Server](https://get.opensuse.org/server), [Rocky Linux Server](https://rockylinux.org) o [Alma Linux Server](https://almalinux.org), entre moltes altres. 
    
    * Utilitzar distribucions que, tot i tenir un entorn gràfic, aquest sigui més minimalista que els entorns estàndard (com GNOME o KDE) i, per tant, més lleuger de recursos. Exemples de distribucions d'aquest tipus són: [Slitaz](https://www.slitaz.org), [Puppy](https://puppylinux-woof-ce.github.io), [Slax](https://www.slax.org), [Bodhi](https://www.bodhilinux.com), [Lubuntu](https://lubuntu.me), [Fedora LXQT](https://fedoraproject.org/spins/lxqt) o fins i tot [Fedora Sway](https://fedoraproject.org/spins/sway), entre moltes altres
       
    En tot cas, si a l'hora de voler crear un escriptori s'optés per triar alguna de les distribucions anteriors, caldria comprovar primer que IsardVDI ofereix la plantilla-base corresponent; si no és així, llavors caldria realitzar manualment la instal·lació de l'escriptori desitjat a partir del medi ISO adient (tal com s'explica [aquí](https://isard.gitlab.io/isardvdi-docs/advanced/media)). 
    
    Si aquest sistema instal·lat es volgués utilitzar, a més, per fer un desplegament, llavors caldria primer crear a partir de l'escriptori anterior una plantilla pròpia (tal com s'explica [aquí](https://isard.gitlab.io/isardvdi-docs/advanced/templates/#create)) i tot seguit fer el desplegament a partir d'ella (tal com s'explica [aquí](https://isard.gitlab.io/isardvdi-docs/advanced/deployments)).

!!! tip "Utilitza el visor SPICE per millorar la teva experiència d'usuari"

    A l'hora de triar un visor per a les teves màquines, et recomanem que utilitzis el visor SPICE ja que ofereix, a més de molt poca latència, una major funcionalitat i flexibilitat que els altres visors disponibles. Per exemple, mitjançant aquest visor podràs gaudir de l'àudio integrat i seràs capaç de copiar-pegar entre la teva màquina real local i les màquines Isard (si no són de tipus "Server"), així com també connectar-hi dispositius USB en la primera i que siguin visibles en les segones, entre altres possibilitats. Per saber més informació sobre les característiques dels diferents visors disponibles, pots consultar [el manual](https://isard.gitlab.io/isardvdi-docs/user/viewers/viewers)

Per realitzar aquesta pràctica només es necessitarà disposar d'una màquina virtual. En aquest cas, però, aquesta no es podrà implementar en forma de desplegament ofert pel professor al grup-clase sinó que caldrà que sigui un escriptori generat per cada alumne individualment. La raó és perquè caldrà que l'alumne pugui editar característiques de la màquina donada (concretament, l'ordre d'arranc dels seus dispositius d'emmagatzematge) i aquesta possibilitat només la tenim en escriptoris propis. En tot cas, teniu més informació sobre com crear desplegaments i les possibilitats que aquests ofereixen [aquí](https://isard.gitlab.io/isardvdi-docs/advanced/deployments)


### Creació de l'escriptori

A continuació es detallen, doncs, els passos que haurà de seguir l'alumnat per crear la màquina de treball en forma d'escriptori. En tot cas, teniu més informació sobre com crear escriptoris i les possibilitats que aquests ofereixen [aquí](https://isard.gitlab.io/isardvdi-docs/user/create_desktop):

1. Pitjar en el botó "Escriptori nou"
2. Triar, al formulari web que apareix, les característiques que desitjem que tingui la màquina. En destacarem les que són rellevants per aquesta pràctica:
      + **vCPUs** i **RAM**: És important tenir en compte el que s'indica al primer quadre d'aquesta pàgina
      + **Plantilla base**: Qualsevol de tipus Linux, com les que proporciona IsardVDI ("Ubuntu 22.04 Desktop", "Debian 12 Server", "Fedora 38 Workstation") o altres que poguem tenir ja fetes (teniu més informació sobre com crear plantilles pròpies [aquí](https://isard.gitlab.io/isardvdi-docs/advanced/templates.ca). És important tenir en compte, però, el que s'indica al segon quadre d'aquesta pàgina 
      + **Visors**: És important tenir en compte el que s'indica al tercer quadre d'aquesta pàgina
      + **Boot**: CD/DVD (molt important!; si no s'indica, la màquina arrencarà sempre des del seu sistema instal·lat en el disc dur tant se val l'arxiu Iso que se li indiqui) 
      + **Mitjans (Isos)**: Qualsevol de tipus Linux que sigui "Live" (és a dir, que arrenqui directament un entorn de treball interactiu, no pas cap assistent d'instal·lació), com els que proporciona IsardVDI ("GParted Live ISO", "Fedora Workstation 38") o qualsevol altre que haguem compartit prèviament amb el grup-classe (veure nota inferior)
3. Pitjar el botó "Crear"

!!! info "Sobre la compartició de mitjans (Isos)"
      
    Com a professor es té la possibilitat de compartir els arxius Iso que volguem amb els nostres alumnes per a què els puguin utilitzar. Només cal seguir les passes indicades [aquí](https://isard.gitlab.io/isardvdi-docs/advanced/media.ca), les quals, molt resumidament, consisteixen en omplir el formulari web disponible pitjant sobre el botó "Nou mitjà" que apareix a l'apartat "Mitjans" del menú superior d'IsardVDI, formulari que demana, com a dades més rellevants, la URL de l'arxiu Iso en qüestió (per tant, aquest arxiu hauria d'estar accessible públicament des d'algun servidor web...de moment IsardVDI no permet la possibilitat de pujar-hi arxius Isos locals) i la llista d'alumnes individuals o grup-classe complerts pels quals es compartirà el mitjà en qüestió (i per tant, ja el tindran disponible per triar durant la creació dels seus escriptoris)

!!! warning "IsardVDI no permet encara afegir discos extra a un escriptori"
    
    És per això que es proposa fer la pràctica en el propi disc vinculat a la plantilla triada, on existeix com a mínim la partició del sistema preinstal·lat. Justament per això, cal arrencar l'escriptori des d'un arxiu Iso en comptes de des del sistema preinstal·lat: per poder tenir accés a tot el disc sense restriccions i poder-lo manipular tal com vulguem; si arrenquéssim des del sistema preinstal·lat, la pròpia partició on resideix aquest sistema ("la partició arrel") no podria ser modificada (és a dir, ni engrandida, ni encongida, ni reformatada, ni esborrada, etc) perquè estaria fent-se servir "en calent" (o dit d'una altra manera, perquè estaria "muntada"), i això ens limitaria molt la nostra capacitat d'acció sobre el disc (però no sobre altres possibles discos extra, si n'hi haguessin). Cal dir, no obstant, que existeix una tecnologia avançada de particionament que sí que permet la manipulació de particions en calent anomenada [LVM](https://gitlab.com/lvmteam/lvm2), però no la veurem en aquesta pràctica
    

## Enunciat de la pràctica

L'alumne/a arrencarà la seva màquina amb la configuració tal com s'ha indicat a l'apartat anterior (és a dir, la màquina estarà formada a partir d'una plantilla base que pot ser Debian, Ubuntu o Fedora, i amb l'ordre d'arranc establert per a què s'inicïi des del CD/DVD, on hi haurem inserit un arxiu Iso que pot ser el de GParted o Fedora Workstation). A partir d'aquí, segons la plantilla base triada i l'arxiu Iso triat, apareixeran diversos escenaris, on serà més còmode fer servir determinades eines de particionament i formatatge envers unes altres. A continuació detallarem el pas a pas en cadascuna de les situacions possibles.

!!! tip "Sobre els discos MBR i GPT"

    Algunes plantilles-base utilitzades en aquesta pràctica (com per exemple la de Debian o Fedora) tenen un esquema de particionament anomenat "MBR", el qual és típic de màquines que tenen com a firmware de reconeixement bàsic de hardware (tot i que siguin virtuals) un de tipus "BIOS". No obstant, a les màquines actuals és més habitual que aquest firmware sigui de tipus "UEFI", amb la qual cosa l'esquema de particionament dels discos emprats és un altre anomenat "GPT" (com passa per exemple amb el disc de la plantilla-base d'Ubuntu). D'entre les diferències més importants entre un disc "MBR" i un disc "GPT" podem destacar-ne dues: que els primers es distingeixen diferents tipus de particions (primàries -només n'hi poden haver fins a 4 màxim-, exteses, lògiques, tot i que en aquesta pràctica d'això no ens preocuparem) mentre que als segons no existeix aquesta distinció, i que en els primers només cal una partició per arrencar un sistema (l'anomenada "partició arrel", que és on es troben emmagatzemats tots els fitxers del propi sistema) mentre que en els segons calen com a mínim dues particions, la partició arrel igual i una altra partició, formatada sempre en el sistema de fitxers FAT32, anomenada "ESP" (d'"EFI System Partition"), on residiran de forma separada el/s gestor/s d'arranc emprat/s per iniciar el sistema en qüestió
    
### Creació, redimensió i formatació de particions 

#### Amb l'eina gràfica GParted (mitjà "GParted Live Iso")

Si el fitxer Iso triat per arrencar la màquina és el "GParted Live Iso", els passos a seguir per tal d'aconseguir un espai lliure al disc on crear i formatejar una nova partició són els següents:

**1.** Iniciem l'escriptori. Apareixerà el següent menú inicial, on triarem l'opció per defecte (o simplement esperarem a que s'activi sola):

![](gestio_particions_assets/gparted/1.general/a.png){style="height:50%;width:50%"}

**2.** A continuació se'ns demanarà triar l'idioma del teclat: podem triar l'opció per defecte ("Don't touch keymap") amb la qual cosa el teclat es configurarà en anglès. Com que en principi no s'escriurà res (tot es farà amb ratolí), es podria triar aquesta opció i prou, però si es vol escollir el teclat espanyol (amb l'opció "Select keymap from arch list", tal com mostra la primera de les captures de pantalla següents), caldrà anar responent a tot un seguit de pantalles que aniran apareixent a partir de llavors. Concretament, són aquestes (les opcions que hi apareixen marcades són les adients a sel·leccionar en cadascuna):

![](gestio_particions_assets/gparted/1.general/b.png){width="45%"}
![](gestio_particions_assets/gparted/1.general/c.png){width="45%"}
![](gestio_particions_assets/gparted/1.general/d.png){width="45%"}
![](gestio_particions_assets/gparted/1.general/e.png){width="45%"}
![](gestio_particions_assets/gparted/1.general/f.png){width="45%"}

**3.** Seguidament, se'ns preguntarà per l'idioma en què volem mostrar els programes. A la captura es veu com hem triat l'idioma "04" (català)

![](gestio_particions_assets/gparted/1.general/g.png){style="height:50%;width:50%"}

**4.** Finalment, se'ns preguntarà si volem iniciar el mode gràfic (resposta per defecte, valor "0") o entrar en un terminal (valor "2"). A la captura es veu com hem triat la primera opció

![](gestio_particions_assets/gparted/1.general/h.png){style="height:50%;width:50%"}

A partir d'aquí, ens trobarem amb un disc el particionament del qual serà una mica diferent segons la plantilla-base emprada, ja que cadascuna incorpora un disc particionat d'una forma diferent. A continuació s'indiquen, doncs, els passos a seguir depenent del què ens trobem:

!!! example "Segons la plantilla-base emprada"

    === "Ubuntu"
      
        Un cop engegat el mode gràfic triat al pas anterior, el primer que ens trobarem serà una pantalla similar a la següent, on es pot veure la finestra, oberta ja, corresponent al programa [Gparted](https://gparted.org) pròpiament dit. 
    
        ![](gestio_particions_assets/gparted/2.ubuntu/a.png){style="height:50%;width:50%"}

        Podem veure que el disc d'aquesta plantilla incorpora una gran partició anomenada "/dev/vda3" majoritàriament buida (en realitat es correspon a la partició arrel del sistema Ubuntu, tot i que això no és necessari saber-ho), a més d'altres com "/dev/vda2" (la partició ESP), "/dev/vda4", etc. L'objectiu de la pràctica serà reduir la mida d'aquesta partició per tal d'obtenir un espai buit on crear-ne una nova partició. Així doncs...
    
        **5.** ... cliquem amb el botó dret sobre la partició que volem redimensionar per tal que aparegui el menú contextual d'opcions i poguem sel·leccionar precisament aquesta acció: redimensionar

        ![](gestio_particions_assets/gparted/2.ubuntu/b.png){style="height:50%;width:50%"}
    
        **6.** Apareixerà un quadre, on podem indicar l'espai lliure (posterior a la partició en qüestió) que volem obtenir a costa de reduir en la mateixa quantitat de MBs la mida d'aquesta partició. A la captura es pot veure que es vol crear un espai de 50GiB.
    
        ![](gestio_particions_assets/gparted/2.ubuntu/c.png){style="height:50%;width:50%"}
    
        I, efectivament, aquest és el resultat:
    
        ![](gestio_particions_assets/gparted/2.ubuntu/d.png){style="height:50%;width:50%"}
    
        **7.** Tot seguit, crearem una nova partició en l'espai buit. Per això, tornem a clicar amb el botó dret, ara sobre l'espai buit en qüestió, per tal que aparegui el menú contextual d'opcions i poguem sel·leccionar l'acció de "nova"
    
        ![](gestio_particions_assets/gparted/2.ubuntu/e.png){style="height:50%;width:50%"}
    
        **8.** Al quadre que hi apareix, podem triar, entre altres detalls, la mida que volem que tingui aquesta nova partició (per defecte ocuparà tot l'espai buit disponible, és a dir, els 50GiB) i el sistema de fitxers amb el qual la volem formatar (és a dir, el tipus d'estructura lògica interna amb què volem bastir la partició per tal de poder començar a introduir-hi dades en forma de carpetes i fitxers). Tal com es pot veure a la captura següent, el programa GParted ofereix un ampli ventall per triar el sistema de fitxers (el "format") desitjat per una partició; d'entre els més utilitzats a sistemes Linux en podem destacar "Ext4" (és el que escollirem) però també "Btrfs" o "Xfs", així com també "Fat32"; d'entre els més utilitzats a sistemes Windows en trobem "Ntfs", "Exfat" i també "Fat32".
    
        ![](gestio_particions_assets/gparted/2.ubuntu/f.png){style="height:50%;width:50%"}

        A la següent captura es pot observar que hem afegit també una etiqueta a la partició (en aquest cas, "Les meves dades"), la qual no és res més que un nom identificador, totalment opcional.
    
        ![](gestio_particions_assets/gparted/2.ubuntu/g.png){style="height:50%;width:50%"}
    
        **9.** A la següent captura es pot veure com ha quedat el disc just en aquest punt on hem arribat 
    
        ![](gestio_particions_assets/gparted/2.ubuntu/h.png){style="height:50%;width:50%"}
    
        No obstant, fins que no cliquem sobre el botó del "check" verd, no s'implementaran de forma efectiva tots els passos anteriors. És per això que en el moment que fem aquest clic, apareixerà el quadre següent preguntant-nos si estem segurs, perquè ja no hi haurà marxa enrera: el disc serà modificat definitivament
    
        ![](gestio_particions_assets/gparted/2.ubuntu/i.png){style="height:50%;width:50%"}
    
        **10.** A la següent captura es pot veure com ha quedat el disc, ara sí, amb tot el procés finalitzat. La nova partició s'anomena "/dev/vda5"
    
        ![](gestio_particions_assets/gparted/2.ubuntu/j.png){style="height:50%;width:50%"}
    
        Ja podrem, doncs, tancar el programa GParted. En desaparèixer la finestra de l'aplicació, se'ns descobriran les diferents icones ubicades a l'entorn d'escriptori minimalista incorporat al sistema Live. Podem fer servir la icona d'"Exit" per apagar la màquina.
    
        ![](gestio_particions_assets/gparted/2.ubuntu/k.png){style="height:50%;width:50%"}

    === "Debian"
    
        Un cop engegat el mode gràfic triat al pas anterior, el primer que ens trobarem serà una pantalla similar a la següent, on es pot veure la finestra, oberta ja, corresponent al programa [Gparted](https://gparted.org) pròpiament dit. 
    
        ![](gestio_particions_assets/gparted/3.debian/a.png){style="height:50%;width:50%"}

        Podem veure que el disc d'aquesta plantilla incorpora una gran partició anomenada "/dev/vda1" majoritàriament buida (en realitat es correspon a la partició arrel del sistema Ubuntu, tot i que això no és necessari saber-ho), a més d'altres com "/dev/vda3", etc. L'objectiu de la pràctica serà reduir la mida d'aquesta partició per tal d'obtenir un espai buit on crear-ne una nova partició. Així doncs...
    
        **5.** ... cliquem amb el botó dret sobre la partició que volem redimensionar per tal que aparegui el menú contextual d'opcions i poguem sel·leccionar precisament aquesta acció: redimensionar

        ![](gestio_particions_assets/gparted/3.debian/b.png){style="height:50%;width:50%"}
    
        **6.** Apareixerà un quadre, on podem indicar l'espai lliure (posterior a la partició en qüestió) que volem obtenir a costa de reduir en la mateixa quantitat de MBs la mida d'aquesta partició. A la captura es pot veure que es vol crear un espai de 50GiB.
    
        ![](gestio_particions_assets/gparted/3.debian/c.png){style="height:50%;width:50%"}
    
        I, efectivament, aquest és el resultat:
    
        ![](gestio_particions_assets/gparted/3.debian/d.png){style="height:50%;width:50%"}
    
        **7.** Tot seguit, crearem una nova partició en l'espai buit. Per això, tornem a clicar amb el botó dret, ara sobre l'espai buit en qüestió, per tal que aparegui el menú contextual d'opcions i poguem sel·leccionar l'acció de "nova"
    
        ![](gestio_particions_assets/gparted/3.debian/e.png){style="height:50%;width:50%"}
    
        **8.** Al quadre que hi apareix, podem triar, entre altres detalls, la mida que volem que tingui aquesta nova partició (per defecte ocuparà tot l'espai buit disponible, és a dir, els 50GiB) i el sistema de fitxers amb el qual la volem formatar (és a dir, el tipus d'estructura lògica interna amb què volem bastir la partició per tal de poder començar a introduir-hi dades en forma de carpetes i fitxers). Tal com es pot veure a la captura següent, el programa GParted ofereix un ampli ventall per triar el sistema de fitxers (el "format") desitjat per una partició; d'entre els més utilitzats a sistemes Linux en podem destacar "Ext4" (és el que escollirem) però també "Btrfs" o "Xfs", així com també "Fat32"; d'entre els més utilitzats a sistemes Windows en trobem "Ntfs", "Exfat" i també "Fat32".
    
        ![](gestio_particions_assets/gparted/3.debian/f.png){style="height:50%;width:50%"}

        A la següent captura es pot observar que hem afegit també una etiqueta a la partició (en aquest cas, "Les meves dades"), la qual no és res més que un nom identificador, totalment opcional.
    
        ![](gestio_particions_assets/gparted/3.debian/g.png){style="height:50%;width:50%"}
    
        **9.** A la següent captura es pot veure com ha quedat el disc just en aquest punt on hem arribat 
    
        ![](gestio_particions_assets/gparted/3.debian/h.png){style="height:50%;width:50%"}
    
        No obstant, fins que no cliquem sobre el botó del "check" verd, no s'implementaran de forma efectiva tots els passos anteriors. És per això que en el moment que fem aquest clic, apareixerà el quadre següent preguntant-nos si estem segurs, perquè ja no hi haurà marxa enrera: el disc serà modificat definitivament
    
        ![](gestio_particions_assets/gparted/3.debian/i.png){style="height:50%;width:50%"}
    
        **10.** A la següent captura es pot veure com ha quedat el disc, ara sí, amb tot el procés finalitzat. La nova partició s'anomena "/dev/vda4"
    
        ![](gestio_particions_assets/gparted/3.debian/j.png){style="height:50%;width:50%"}
    
        Ja podrem, doncs, tancar el programa GParted. En desaparèixer la finestra de l'aplicació, se'ns descobriran les diferents icones ubicades a l'entorn d'escriptori minimalista incorporat al sistema Live. Podem fer servir la icona d'"Exit" per apagar la màquina.
    
        ![](gestio_particions_assets/gparted/3.debian/k.png){style="height:50%;width:50%"}
    
    === "Fedora"
    
        Un cop engegat el mode gràfic triat al pas anterior, el primer que ens trobarem serà una pantalla similar a la següent, on es pot veure la finestra, oberta ja, corresponent al programa [Gparted](https://gparted.org) pròpiament dit. 
    
        ![](gestio_particions_assets/gparted/4.fedora/a.png){style="height:50%;width:50%"}

        Podem veure que el disc d'aquesta plantilla incorpora una gran partició anomenada "/dev/vda3" majoritàriament buida (en realitat es correspon a la partició arrel del sistema Ubuntu, tot i que això no és necessari saber-ho), a més d'altres com "/dev/vda4", etc. L'objectiu de la pràctica serà reduir la mida d'aquesta partició per tal d'obtenir un espai buit on crear-ne una nova partició. Així doncs...
    
        **5.** ... cliquem amb el botó dret sobre la partició que volem redimensionar per tal que aparegui el menú contextual d'opcions i poguem sel·leccionar precisament aquesta acció: redimensionar

        ![](gestio_particions_assets/gparted/4.fedora/b.png){style="height:50%;width:50%"}
    
        **6.** Apareixerà un quadre, on podem indicar l'espai lliure (posterior a la partició en qüestió) que volem obtenir a costa de reduir en la mateixa quantitat de MBs la mida d'aquesta partició. A la captura es pot veure que es vol crear un espai de 50GiB.
    
        ![](gestio_particions_assets/gparted/4.fedora/c.png){style="height:50%;width:50%"}
    
        I, efectivament, aquest és el resultat:
    
        ![](gestio_particions_assets/gparted/4.fedora/d.png){style="height:50%;width:50%"}
    
        **7.** Tot seguit, crearem una nova partició en l'espai buit. Per això, tornem a clicar amb el botó dret, ara sobre l'espai buit en qüestió, per tal que aparegui el menú contextual d'opcions i poguem sel·leccionar l'acció de "nova"
    
        ![](gestio_particions_assets/gparted/4.fedora/e.png){style="height:50%;width:50%"}
    
        **8.** Al quadre que hi apareix, podem triar, entre altres detalls, la mida que volem que tingui aquesta nova partició (per defecte ocuparà tot l'espai buit disponible, és a dir, els 50GiB) i el sistema de fitxers amb el qual la volem formatar (és a dir, el tipus d'estructura lògica interna amb què volem bastir la partició per tal de poder començar a introduir-hi dades en forma de carpetes i fitxers). Tal com es pot veure a la captura següent, el programa GParted ofereix un ampli ventall per triar el sistema de fitxers (el "format") desitjat per una partició; d'entre els més utilitzats a sistemes Linux en podem destacar "Ext4" (és el que escollirem) però també "Btrfs" o "Xfs", així com també "Fat32"; d'entre els més utilitzats a sistemes Windows en trobem "Ntfs", "Exfat" i també "Fat32".
    
        ![](gestio_particions_assets/gparted/4.fedora/f.png){style="height:50%;width:50%"}

        A la següent captura es pot observar que hem afegit també una etiqueta a la partició (en aquest cas, "Les meves dades"), la qual no és res més que un nom identificador, totalment opcional.
    
        ![](gestio_particions_assets/gparted/4.fedora/g.png){style="height:50%;width:50%"}
    
        **9.** A la següent captura es pot veure com ha quedat el disc just en aquest punt on hem arribat 
    
        ![](gestio_particions_assets/gparted/4.fedora/h.png){style="height:50%;width:50%"}
    
        No obstant, fins que no cliquem sobre el botó del "check" verd, no s'implementaran de forma efectiva tots els passos anteriors. És per això que en el moment que fem aquest clic, apareixerà el quadre següent preguntant-nos si estem segurs, perquè ja no hi haurà marxa enrera: el disc serà modificat definitivament
    
        ![](gestio_particions_assets/gparted/4.fedora/i.png){style="height:50%;width:50%"}
    
        **10.** A la següent captura es pot veure com ha quedat el disc, ara sí, amb tot el procés finalitzat. La nova partició s'anomena "/dev/vda5"
    
        ![](gestio_particions_assets/gparted/4.fedora/j.png){style="height:50%;width:50%"}
    
        Ja podrem, doncs, tancar el programa GParted. En desaparèixer la finestra de l'aplicació, se'ns descobriran les diferents icones ubicades a l'entorn d'escriptori minimalista incorporat al sistema Live. Podem fer servir la icona d'"Exit" per apagar la màquina.
    
        ![](gestio_particions_assets/gparted/4.fedora/k.png){style="height:50%;width:50%"}

#### Amb l'eina gràfica Gnome Disks (mitjà "Fedora Workstation 38")

Si el fitxer Iso triat per arrencar la màquina és el de "Fedora Workstation" (o, de fet, el de qualsevol altre sistema Live que arrenqui en l'entorn Gnome, com és per exemple l'arxiu Iso de l'"Ubuntu Desktop"), els passos a seguir per tal d'aconseguir un espai lliure al disc on crear i formatejar una nova partició són els següents:

**1.** Iniciem l'escriptori. Apareixerà el següent menú inicial, on podem triar l'opció per defecte (la qual, abans d'iniciar l'entorn "Live" fa una comprovació del fitxer Iso per assegurar-se que no estigui corrupte) o bé la primera opció "Start Fedora Workstation-Live", que inicia l'entorn "Live" directament:

![](gestio_particions_assets/gdisks/1.general/a.png){style="height:30%;width:50%"}

**2.** A continuació ens apareixerà la finestra següent, on pitjarem sobre el botó "Not now" perquè no volem iniciar la instal·lació en disc de cap sistema Fedora sinó simplement iniciar un entorn "Live"

![](gestio_particions_assets/gdisks/1.general/b.png){style="height:30%;width:50%"}

Fent-ho així, efectivament, haurem iniciat automàticament una sessió "Live" dins de l'entorn Gnome (que és l'escriptori per defecte de Fedora Workstation, el qual incorpora el programa que farem servir, el [Gnome Disks](https://gitlab.gnome.org/GNOME/gnome-disk-utility))

![](gestio_particions_assets/gdisks/1.general/c.png){style="height:30%;width:50%"}

**3.** La manera més fàcil d'accedir a Gnome Disk és pitjant sobre la cantonada superior esquerra de la pantalla (allà on posa "Activities") i, a la caixa de recerca que llavors apareixerà a la zona central superior de l'escriptori, escriure-hi el nom "disks": tal com es mostra a la captura següent, haurà d'aparèixer la icona corresponent al programa que desitjem iniciar.

![](gestio_particions_assets/gdisks/1.general/d.png){style="height:50%;width:50%"}

Només caldrà pitjar sobre aquesta icona i ja tindrem el Gnome Disks en marxa
 
![](gestio_particions_assets/gdisks/1.general/e.png){style="height:50%;width:50%"} 

A partir d'aquí, ens trobarem amb un disc el particionament del qual serà una mica diferent segons la plantilla-base emprada, ja que cadascuna incorpora un disc particionat d'una forma diferent. A continuació s'indiquen, doncs, els passos a seguir depenent del què ens trobem:

!!! example "Segons la plantilla-base emprada"

    === "Ubuntu"
    
        **4.** Pitjarem, en la llista de dispositius mostrada a l'esquerra de la finestra, sobre el ítem que representa el disc de la màquina (identificat per "107 GB Hard Disk"; els altres dispositius són la lectora de CD/DVD i dispositius virtuals generats pel propi sistema "Live"). D'aquesta manera, se'ns mostrarà en la zona central el seu esquema de particionament actual. Allà hi pitjarem sobre la partició que volem redimensionar en concret, per tal que aparegui "activa" d'un altre color; això indica que totes les operacions que fem a partir d'ara seran realitzades sobre aquesta partició en concret.
        
        ![](gestio_particions_assets/gdisks/2.ubuntu/a.png){style="height:50%;width:50%"} 
         
        **5.** Amb la partició adient sel·leccionada, pitjarem sobre el botó de les rodes dentades per a què hi apareguin les diferents accions que hi podem fer sobre aquesta partició. De totes elles, triarem l'opció "Resize..."
        
        ![](gestio_particions_assets/gdisks/2.ubuntu/b.png){style="height:50%;width:50%"} 
        
        **6.** Ens apareixerà la següent finestra emergent, on simplement haurem de pitjar al botó "Authenticate" per tal d'obtenir automàticament els permisos que ens permetran realitzar la tasca a fer
        
        ![](gestio_particions_assets/gdisks/2.ubuntu/c.png){style="height:50%;width:50%"}

        **7.** Tot seguit ens apareixerà la següent finestra, on haurem de decidir justament la mida que volem per la partició. A la captura es pot observar com hem indicat que volem deixar 50GB d'espai lliure després d'ella  
        
        ![](gestio_particions_assets/gdisks/2.ubuntu/d.png){style="height:50%;width:50%"}
        
        **8.** Ja tindrem, doncs, l'espai buit creat, tal com mostra la següent captura. El pas següent és crear una nova partició en aquest espai, cosa que podem fer, com també mostra la següent captura, pitjant sobre el botó amb el símbol del "+".
        
        ![](gestio_particions_assets/gdisks/2.ubuntu/e.png){style="height:50%;width:50%"}
        
        **9.** Ens apareixerà una primera finestra on podrem establir la mida d'aquesta nova partició (per defecte ocuparà tot l'espai buit que hem deixat, és a dir, 50GB):
        
        ![](gestio_particions_assets/gdisks/2.ubuntu/f.png){style="height:50%;width:50%"}
              
        **10.** Seguidament, ens apareixerà una segona finestra on podrem establir bàsicament dues característiques més de la nova partició: la seva etiqueta ("Les meves dades"; recodem que una etiqueta és tan sols un nom identificador, totalment opcional, que podem assignar a la partició) i el sistema de fitxers amb el qual es formatarà (hi ha diferents opcions a triar: en aquesta pràctica escollirem l'opció per defecte, "Internal disk for use with Linux system only (Ext4)", però també hi hauria la possibilitat de triar el format FAT32, o el format NTFS -més propi de sistemes Windows-, o el format Ext4 combinat amb [LUKS](https://gitlab.com/cryptsetup/cryptsetup) per disposar de particions xifrades amb contrasenya, o altres, a especificar manualment)
        
        ![](gestio_particions_assets/gdisks/2.ubuntu/g.png){style="height:50%;width:50%"}
        
        **11.** La captura següent mostra el resultat obtingut fins aquí:
        
        ![](gestio_particions_assets/gdisks/2.ubuntu/h.png){style="height:50%;width:50%"}
    
        Ja hem acabat, doncs: ja podem tancar el programa i continuar en la sessió "Live" per probar altres aplicacions incorporades de sèrie en l'escriptori Gnome o bé apagar el sistema (pitjant al botó superior dret de la pantalla) 
        
        ![](gestio_particions_assets/gdisks/2.ubuntu/i.png){style="height:50%;width:50%"}
    
    === "Debian"
    
        **4.** Pitjarem, en la llista de dispositius mostrada a l'esquerra de la finestra, sobre el ítem que representa el disc de la màquina (identificat per "107 GB Hard Disk"; els altres dispositius són la lectora de CD/DVD i dispositius virtuals generats pel propi sistema "Live"). D'aquesta manera, se'ns mostrarà en la zona central el seu esquema de particionament actual. Allà hi pitjarem sobre la partició que volem redimensionar en concret, per tal que aparegui "activa" d'un altre color; això indica que totes les operacions que fem a partir d'ara seran realitzades sobre aquesta partició en concret.
        
        ![](gestio_particions_assets/gdisks/3.debian/a.png){style="height:50%;width:50%"} 
         
        **5.** Amb la partició adient sel·leccionada, pitjarem sobre el botó de les rodes dentades per a què hi apareguin les diferents accions que hi podem fer sobre aquesta partició. De totes elles, triarem l'opció "Resize..."
        
        ![](gestio_particions_assets/gdisks/3.debian/b.png){style="height:50%;width:50%"} 
        
        **6.** Ens apareixerà la següent finestra emergent, on simplement haurem de pitjar al botó "Authenticate" per tal d'obtenir automàticament els permisos que ens permetran realitzar la tasca a fer
        
        ![](gestio_particions_assets/gdisks/3.debian/c.png){style="height:50%;width:50%"}

        **7.** Tot seguit ens apareixerà la següent finestra, on haurem de decidir justament la mida que volem per la partició. A la captura es pot observar com hem indicat que volem deixar 50GB d'espai lliure després d'ella  
        
        ![](gestio_particions_assets/gdisks/3.debian/d.png){style="height:50%;width:50%"}
        
        **8.** Ja tindrem, doncs, l'espai buit creat, tal com mostra la següent captura. El pas següent és crear una nova partició en aquest espai, cosa que podem fer, com també mostra la següent captura, pitjant sobre el botó amb el símbol del "+".
        
        ![](gestio_particions_assets/gdisks/3.debian/e.png){style="height:50%;width:50%"}
        
        **9.** Ens apareixerà una primera finestra on podrem establir la mida d'aquesta nova partició (per defecte ocuparà tot l'espai buit que hem deixat, és a dir, 50GB):
        
        ![](gestio_particions_assets/gdisks/3.debian/f.png){style="height:50%;width:50%"}
              
        **10.** Seguidament, ens apareixerà una segona finestra on podrem establir bàsicament dues característiques més de la nova partició: la seva etiqueta ("Les meves dades"; recodem que una etiqueta és tan sols un nom identificador, totalment opcional, que podem assignar a la partició) i el sistema de fitxers amb el qual es formatarà (hi ha diferents opcions a triar: en aquesta pràctica escollirem l'opció per defecte, "Internal disk for use with Linux system only (Ext4)", però també hi hauria la possibilitat de triar el format FAT32, o el format NTFS -més propi de sistemes Windows-, o el format Ext4 combinat amb [LUKS](https://gitlab.com/cryptsetup/cryptsetup) per disposar de particions xifrades amb contrasenya, o altres, a especificar manualment)
        
        ![](gestio_particions_assets/gdisks/3.debian/g.png){style="height:50%;width:50%"}
        
        **11.** La captura següent mostra el resultat obtingut fins aquí:
        
        ![](gestio_particions_assets/gdisks/3.debian/h.png){style="height:50%;width:50%"}
    
        Ja hem acabat, doncs: ja podem tancar el programa i continuar en la sessió "Live" per probar altres aplicacions incorporades de sèrie en l'escriptori Gnome o bé apagar el sistema (pitjant al botó superior dret de la pantalla) 
        
        ![](gestio_particions_assets/gdisks/3.debian/i.png){style="height:50%;width:50%"}
    
    === "Fedora"
    
        Desgraciadament, l'eina Gnome Disks no és capaç de redimensionar sistemes de fitxers de tipus Btrfs, que és justament el format emprat per defecte en les instal·lacions dels sistemes Fedora (és a dir, el format de la partició "/dev/vda3"). Així que amb aquesta combinació d'eina<->plantilla-base no podrem continuar realitzant la pràctica 

Cal tenir en compte que Gnome Disks és un programa que permet realitzar moltes altres accions sobre les particions individuals (disponibles en general a partir del botó amb el símbol de les rodes dentades) però també sobre el disc en general (mitjançant les opcions disponibles en el botó dels tres punts verticals ubicat a la part superior dreta de la finestra


#### Amb una combinació d'eines de terminal (qualsevol mitjà)

Una forma alternativa d'aconseguir els mateixos objectius que amb el programa GParted o amb el programa Gnome Disks és fent ús de comandes de terminal especialitzades (de fet, la funcionalitat de les eines gràfiques anteriors sovint se sustenta en aquestes comandes de terminal, justament, executades com a "backend"). Per treballar d'aquesta manera només ens caldrà un terminal arrencat des d'un sistema "Live", el qual tant pot ser el de la Iso GParted com el de la Iso Fedora Workstation (com el de qualsevol altra distribució, de fet). Així doncs, segons la Iso triada haurem de realitzar els següents passos per arribar a un terminal interactiu:

!!! example "Segons el mitjà emprat"
 
    === "GParted Live ISO"
    
        **1.** Iniciem l'escriptori. Apareixerà el següent menú inicial, on triarem l'opció per defecte (o simplement esperarem a que s'activi sola):

        ![](gestio_particions_assets/gparted/1.general/a.png){style="height:50%;width:50%"}

        **2.** A continuació se'ns demanarà triar l'idioma del teclat: podem triar l'opció per defecte ("Don't touch keymap") amb la qual cosa el teclat es configurarà en anglès. Com que en principi no s'escriurà res (tot es farà amb ratolí), es podria triar aquesta opció i prou, però si es vol escollir el teclat espanyol (amb l'opció "Select keymap from arch list", tal com mostra la primera de les captures de pantalla següents), caldrà anar responent a tot un seguit de pantalles que aniran apareixent a partir de llavors. Concretament, són aquestes (les opcions que hi apareixen marcades són les adients a sel·leccionar en cadascuna):

        ![](gestio_particions_assets/gparted/1.general/b.png){width="45%"}
        ![](gestio_particions_assets/gparted/1.general/c.png){width="45%"}
        ![](gestio_particions_assets/gparted/1.general/d.png){width="45%"}
        ![](gestio_particions_assets/gparted/1.general/e.png){width="45%"}
        ![](gestio_particions_assets/gparted/1.general/f.png){width="45%"}

        **3.** Seguidament, se'ns preguntarà per l'idioma en què volem mostrar els programes. A la captura es veu com hem triat l'idioma "04" (català)

        ![](gestio_particions_assets/gparted/1.general/g.png){style="height:50%;width:50%"}

        **4.** Finalment, se'ns preguntarà si volem iniciar el mode gràfic (resposta per defecte, valor "0") o entrar en un terminal (valor "2"). Justament aquí és on haurem de triar explícitament la darrera opció, tal com es mostra a la captura de pantalla següent:
        
        ![](gestio_particions_assets/gparted/1.general/h2.png){style="height:50%;width:50%"}

        Entrarem en un terminal directament com a usuari administrador "root"

    === "Fedora Workstation 38"
    
        **1.** Iniciem l'escriptori. Apareixerà el següent menú inicial, on podem triar l'opció per defecte (la qual, abans d'iniciar l'entorn "Live" fa una comprovació del fitxer Iso per assegurar-se que no estigui corrupte) o bé la primera opció "Start Fedora Workstation-Live", que inicia l'entorn "Live" directament:

        ![](gestio_particions_assets/gdisks/1.general/a.png){style="height:30%;width:50%"}

        **2.** A continuació ens apareixerà la finestra següent, on pitjarem sobre el botó "Not now" perquè no volem iniciar la instal·lació en disc de cap sistema Fedora sinó simplement iniciar un entorn "Live"

        ![](gestio_particions_assets/gdisks/1.general/b.png){style="height:30%;width:50%"}

        Fent-ho així, efectivament, haurem iniciat automàticament una sessió "Live" dins de l'entorn Gnome (que és l'escriptori per defecte de Fedora Workstation).

        **3.**  A partir d'aquí només hem d'anar a buscar un programa que ens doni accés al terminal. La manera més fàcil per fer això és pitjant sobre la cantonada superior esquerra de la pantalla (allà on posa "Activities") i, a la caixa de recerca que llavors apareixerà a la zona central superior de l'escriptori, escriure-hi la paraula "terminal": tal com es mostra a la captura següent, haurà d'aparèixer la icona corresponent a un programa:

        ![](gestio_particions_assets/terminal/1.general/a.png){style="height:50%;width:50%"} 

        Només caldrà pitjar sobre aquesta icona i ja tindrem el terminal a la vista:
 
        ![](gestio_particions_assets/terminal/1.general/b.png){style="height:50%;width:50%"} 

        **4.** No obstant, per defecte aquest terminal s'obre amb els permisos de l'usuari que ha iniciat la sessió d'escriptori "Live", que és un usuari amb molts pocs permisos. Per realitzar les tasques requerides per la pràctica, necessitem que l'usuari de treball sigui l'administrador del sistema (l'usuari "root"). Per convertir-nos en ell, només caldrà executar al terminal la comanda següent (tal com mostra també la captura següent):
        
        ```
        $ sudo -i
        ```
        
        ![](gestio_particions_assets/terminal/1.general/c.png){style="height:50%;width:50%"} 

        !!! info "Sobre l'idioma predeterminat del teclat"
        
            És molt probable que l'idioma del teclat de la sessió "Live" del Fedora Workstation estigui definit per defecte com l'anglès. Això és un problema perquè la disposició de les tecles d'aquest teclat no es correspon a la de les tecles del teclat que fem servir normalment nosaltres, que és el definit amb l'idioma espanyol. Per sol·lucionar aquest problema, es pot anar a l'apartat "Keyboard" del panell de control del Gnome i allà afegir (amb el botó "+") una altra "Input Source", que serà l'espanyola (valor "Spanish (Spain)"). A més, haurem d'eliminar la que hi apareixia per defecte, l'anglesa, fent servir l'opció apropiada del menú que apareix en pitjar el botó adient dels tres punts verticals. I ja està, el canvi s'aplicarà immediatament
            
Un cop ja estem dins d'un terminal interactiu "live", ja podrem manipular el disc de la màquina. Però com que el seu particionament serà una mica diferent segons la plantilla-base emprada (ja que cadascuna incorpora un disc particionat d'una forma diferent), a continuació s'indiquen els passos a seguir depenent del què ens trobem:

!!! example "Segons la plantilla-base emprada"

    === "Ubuntu 22.04 Desktop"
    
        **1.** Observarem primer l'estructura de particions dels dispositius de blocs (discos, CD/DVD...) reconeguts pel sistema mitjançant la següent comanda
       
        ```
        # lsblk
        ```
    
        Tal com es pot veure a la captura següent, corresponent a la sortida de la comanda anterior, a més d'altres dispositius que no ens interessen (com els de tipus "loop" o "zram", per ús intern del sistema "Live", o la pròpia lectora de CD/DVD, "sr0"), en la plantilla utilitzada hi ha un disc anomenat "vda" de 100GB on destaca de molt per la seva mida la partició "vda3". Aquesta serà la partició que encongirem
       
        ![](gestio_particions_assets/terminal/2.ubuntu/a.png){style="height:50%;width:50%"} 
       
        !!! note "Trobar més informació sobre una partició"

            La comanda *lsblk* ens pot proporcionar molta més informació sobre les particions detectades als discos de la màquina, més enllà de la seva mida i eventual punt de muntatge. Per exemple, ens pot mostrar el sistema de fitxers de cada partició així com també la quantitat d'espai utilitzat i lliure en cadascuna d'elles. Això es pot aconseguir especificant individualment les columnes que es volen mostrar a la seva sortida amb el paràmetre -o (en lloc de quedar-nos amb les columnes que se'ns mostra per defecte). Concretament, la següent comanda mostraria, cada partició o disc, el seu nom, si és partició o disc, la seva mida, els seus eventuals punts de muntatge, el seu sistema de fitxers ("format"), la mida del seu sistema de fitxers (que normalment coincidirà amb la mida de la partició, tal com de seguida explicarem), la quantitat d'espai utilitzat, la quantitat d'espai lliure i el percentatge d'espai utilitzat:
            
            ```
            # lsblk -o NAME,TYPE,SIZE,MOUNTPOINTS,FSTYPE,FSSIZE,FSUSED,FSAVAIL,FSUSE%
            ```

            Una altra comanda que mostra una informació similar a l'anterior és:
            
            ```
            # df -hT
            ```
            
            No obstant, les comandes anteriors mostren el sistema de fitxers i la quantitat d'espai utilitzat només de les particions que estan muntades en aquest moment, de les que no, no. Per tant, en el cas de la nostra pràctica, on el disc de treball no té cap partició muntada, no les podrem fer servir. Afortunadament, tenim alternatives. La següent comanda ens mostra el sistema de fitxers d'una partició (com per exemple, en el nostre cas, "/dev/vda3") sense que aquesta calgui que estigui muntada:
        
            ```
            # file -s /dev/vda3
            ``` 
        
            D'altra banda, la següent comanda mostra la quantitat de blocs totals ("Block count") i lliures ("Free blocks") del sistema de fitxers en qüestió. Cal tenir en compte que la mida en bytes del "bloc" ve donada pel valor "Block size" i, sobre tot, que aquesta comanda només funciona en particions formatejades en Ext2/Ext3/Ext4 (cada sistema de fitxers vindrà acompanyat de la seva comanda associada adient per obtenir aquest tipus d'informació; haurem de conèixer quina és en cada cas)
        
            ```
            # tune2fs -l /dev/vda3
            ```
       
        **2.** Un cop reconeguda la partició d'interès com la "/dev/vda3", procedirem a encongir-la. Aquí cal tenir en compte un concepte molt important, que és que no és el mateix la partició en sí (que no és res més que una "caixa" buida reconeguda per Linux amb un inici i un final dins del disc) que el sistema de fitxers que conté (el qual representa la seva estructura interna). Això a la pràctica significa que hem de redimensionar les dues coses: en el cas d'engrandir, primer ho faríem amb la partició i seguidament, per poder fer ús efectiu d'aquest nou espai, ho faríem amb el sistema de fitxers; en el cas d'encongir (que és el cas d'aquesta pràctica), primer encongirem el sistema de fitxers per tal què pugui caber en la nova mida que tindrà la partició  (això ho podrem fer, òbviament, sempre i quan encara hi hagi espai sense utilitzar dins del sistema de fitxers que ens permeti reduir la seva mida sense problema), la qual encongirem a continuació en un segon pas.
        
        Així doncs, encongirem 50GB el sistema de fitxers de "/dev/vda3" amb la següent comanda (la qual, atenció, només funciona en particions formatejades en Ext2/Ext3/Ext4; en el cas de tenir un altre sistema de fitxers, caldria conèixer quina seria la comanda equivalent), tenint en compte que la seva mida original era de prop de 80GB, tal com hem vist a la sortida de la comanda *lsblk* anterior (el valor numèric que s'indica a *resize2fs* es correspon a la mida *final* que volem que tingui el sistema de fitxers de la partició indicada, independentment de la mida que tingués originalment):

        ```
        # resize2fs /dev/vda3 30G
        ```

        !!! note ""
            És molt probable que, abans d'executar la comanda anterior, se't demani executar-ne la següent:
            
            ```
            # fsck.ext4 -f /dev/vda3
            ```
            
            Aquesta comanda prèvia serveix per comprovar que el sistema de fitxers (en aquest cas, específicament de tipus Ext4 però existeixen variants *fsck* per la gran majoria de sistemes de fitxers) no estigui corrupte i, en cas que en detecti algun problema (fitxers inexistents que hi haurien de constar, fitxers existents dels quals no es té referència, etc), intentar resoldre'l.

        **3.** El següent pas, com hem dit, un cop encongit el sistema de fitxers, és encongir "el contenidor" on es troba, és a dir, la partició. Aquest pas és necessari perquè si no el féssim, hi haurà un espai inútil sense poder utilitzar, ja que per una banda no el podríem fer servir per emmagatzemar dades (ja que no estaria formatejat) però per l'altra estaria bloquejat per fer res en ell perquè constaria que aquest espai pertany a una determinada partició ja existent, i el que volem precisament és crear-ne una de nova en aquest espai sense format.
        
        Una de les eines de gestió de particions més coneguda és *fdisk*. No obstant, no té cap opció per redimensionar particions, així que el mètode a utilitzar serà eliminar la partició actual i crear-ne una de nova començant pel mateix lloc que l'antiga amb una mida més petita (englobant tot el sistema de fitxers encongit prèviament, això sí). Si sorprén el fet que eliminar una partició no esborra la informació, hem de tenir present que la informació està emmagatzemada al sistema de fitxers i que les particions només són el mètode per accedir-hi: el que estem fent, doncs, és deixar la informació temporalment inaccessible per tot seguit ubicar-la de nou. Així doncs, el procés serà el següent: primer executarem la comanda *fdisk* sobre el disc on treballarem...:
        
        ```
        # fdisk /dev/vda
        ```

        ...i això ens portarà a un entorn interactiu on haurem d'indicar les opcions adients. Concretament:
        
        * La tecla 'p' (de *'print'*) ens mostra l'estructura de particions del disc
        
        ![](gestio_particions_assets/terminal/2.ubuntu/b.png){style="height:50%;width:50%"} 
        
        * La tecla 'd' de (*'delete'*) ens permetrà eliminar la partició triada a continuació (en el nostre cas, la nº3)
        
        ![](gestio_particions_assets/terminal/2.ubuntu/c.png){style="height:50%;width:50%"} 
        
        * La tecla 'n' de (*'new'*) ens permetrà crear una nova partició, la qual indicarem que sigui la nº3 de nou, que comenci al mateix sector que començava l'anterior partició nº3 però que acabi, atenció, no al darrer sector disponible (perquè llavors estaríem recreant de nou una partició amb la mateixa mida que l'antiga) sinó que tingui "només" 30G, que és justament l'espai amb el què hem encongit el sistema de fitxers; noteu que per indicar mida en lloc d'un número final de sector cal precedir la xifra pel símbol "+". A la pregunta de si volem esborrar la "signatura" de la partició pretèrita podem contestar que sí. 
        
        * La tecla 'w' de (*'write'*) cal prèmer-la per a què tots aquest canvis es facin efectius al disc
        
        ![](gestio_particions_assets/terminal/2.ubuntu/d.png){style="height:50%;width:50%"} 

        **4.** Un cop ja tenim l'espai buit per poder crear una nova partició, ho farem, de nou utilitzant l'eina *fdisk*, de forma similar al punt anterior:
        
        ```
        # fdisk /dev/vda
        ```

        Ara haurem de prèmer només la tecla 'n' (per crear una nova partició, la qual, en aquest cas, serà la nº5 perquè és la primera xifra disponible i tindrà com a primer i darrer sector del disc el que la pròpia eina ens proposi, ja que seran el primer i darrer sector disponible d'espai buit, respectivament) i la tecla 'w' (per escriure els canvis en disc)
        
        ![](gestio_particions_assets/terminal/2.ubuntu/e.png){style="height:50%;width:50%"}
             
        Podem confirmar, ja des de fora de la consola interactiva interna de l'eina *fdisk*, que, efectivament, tenim una nova partició ("/dev/vda5") amb una mida de prop de 50GB, tal com volíem, executant directament la comanda següent (equivalent a la tecla 'p' dins de la consola interactiva):
        
        ```
        # fdisk -l /dev/vda
        ```

        ![](gestio_particions_assets/terminal/2.ubuntu/f.png){style="height:50%;width:50%"}

        **5.** Finalment, li haurem d'assignar un sistema de fitxers a aquesta nova partició (és a dir, l'haurem de "formatar"). Això ho podem aconseguir fàcilment amb la següent comanda, la qual formatarà la partició indicada concretament en Ext4 (tot i que la pròpia comanda *mkfs* és capaç de formatar en molts altres formats diferents, si així ho desitjem), afegint-hi una etiqueta:
        
        ```
        # mkfs.ext4 -L "Les meves dades" /dev/vda5
        ```

    === "Debian 12 Server"
    
        **1.** Observarem primer l'estructura de particions dels dispositius de blocs (discos, CD/DVD...) reconeguts pel sistema mitjançant la següent comanda
       
        ```
        # lsblk
        ```
    
        Tal com es pot veure a la captura següent, corresponent a la sortida de la comanda anterior, a més d'altres dispositius que no ens interessen (com els de tipus "loop" o "zram", per ús intern del sistema "Live", o la pròpia lectora de CD/DVD, "sr0"), en la plantilla utilitzada hi ha un disc anomenat "vda" de 100GB on destaca de molt per la seva mida la partició "vda1". Aquesta serà la partició que encongirem
       
        ![](gestio_particions_assets/terminal/3.debian/a.png){style="height:50%;width:50%"} 
       
        !!! note "Trobar més informació sobre una partició"

            La comanda *lsblk* ens pot proporcionar molta més informació sobre les particions detectades als discos de la màquina, més enllà de la seva mida i eventual punt de muntatge. Per exemple, ens pot mostrar el sistema de fitxers de cada partició així com també la quantitat d'espai utilitzat i lliure en cadascuna d'elles. Això es pot aconseguir especificant individualment les columnes que es volen mostrar a la seva sortida amb el paràmetre -o (en lloc de quedar-nos amb les columnes que se'ns mostra per defecte). Concretament, la següent comanda mostraria, cada partició o disc, el seu nom, si és partició o disc, la seva mida, els seus eventuals punts de muntatge, el seu sistema de fitxers ("format"), la mida del seu sistema de fitxers (que normalment coincidirà amb la mida de la partició, tal com de seguida explicarem), la quantitat d'espai utilitzat, la quantitat d'espai lliure i el percentatge d'espai utilitzat:
            
            ```
            # lsblk -o NAME,TYPE,SIZE,MOUNTPOINTS,FSTYPE,FSSIZE,FSUSED,FSAVAIL,FSUSE%
            ```

            Una altra comanda que mostra una informació similar a l'anterior és:
            
            ```
            # df -hT
            ```
            
            No obstant, les comandes anteriors mostren el sistema de fitxers i la quantitat d'espai utilitzat només de les particions que estan muntades en aquest moment, de les que no, no. Per tant, en el cas de la nostra pràctica, on el disc de treball no té cap partició muntada, no les podrem fer servir. Afortunadament, tenim alternatives. La següent comanda ens mostra el sistema de fitxers d'una partició (com per exemple, en el nostre cas, "/dev/vda1") sense que aquesta calgui que estigui muntada:
        
            ```
            # file -s /dev/vda1
            ``` 
        
            D'altra banda, la següent comanda mostra la quantitat de blocs totals ("Block count") i lliures ("Free blocks") del sistema de fitxers en qüestió. Cal tenir en compte que la mida en bytes del "bloc" ve donada pel valor "Block size" i, sobre tot, que aquesta comanda només funciona en particions formatejades en Ext2/Ext3/Ext4 (cada sistema de fitxers vindrà acompanyat de la seva comanda associada adient per obtenir aquest tipus d'informació; haurem de conèixer quina és en cada cas)
        
            ```
            # tune2fs -l /dev/vda1
            ```
       
        **2.** Un cop reconeguda la partició d'interès com la "/dev/vda1", procedirem a encongir-la. Aquí cal tenir en compte un concepte molt important, que és que no és el mateix la partició en sí (que no és res més que una "caixa" buida reconeguda per Linux amb un inici i un final dins del disc) que el sistema de fitxers que conté (el qual representa la seva estructura interna). Això a la pràctica significa que hem de redimensionar les dues coses: en el cas d'engrandir, primer ho faríem amb la partició i seguidament, per poder fer ús efectiu d'aquest nou espai, ho faríem amb el sistema de fitxers; en el cas d'encongir (que és el cas d'aquesta pràctica), primer encongirem el sistema de fitxers per tal què pugui caber en la nova mida que tindrà la partició  (això ho podrem fer, òbviament, sempre i quan encara hi hagi espai sense utilitzar dins del sistema de fitxers que ens permeti reduir la seva mida sense problema), la qual encongirem a continuació en un segon pas.
        
        Així doncs, encongirem 50GB el sistema de fitxers de "/dev/vda1" amb la següent comanda (la qual, atenció, només funciona en particions formatejades en Ext2/Ext3/Ext4; en el cas de tenir un altre sistema de fitxers, caldria conèixer quina seria la comanda equivalent), tenint en compte que la seva mida original era de prop de 80GB, tal com hem vist a la sortida de la comanda *lsblk* anterior (el valor numèric que s'indica a *resize2fs* es correspon a la mida *final* que volem que tingui el sistema de fitxers de la partició indicada, independentment de la mida que tingués originalment):

        ```
        # resize2fs /dev/vda1 30G
        ```

        !!! note ""
            És molt probable que, abans d'executar la comanda anterior, se't demani executar-ne la següent:
            
            ```
            # fsck.ext4 -f /dev/vda1
            ```
            
            Aquesta comanda prèvia serveix per comprovar que el sistema de fitxers (en aquest cas, específicament de tipus Ext4 però existeixen variants *fsck* per la gran majoria de sistemes de fitxers) no estigui corrupte i, en cas que en detecti algun problema (fitxers inexistents que hi haurien de constar, fitxers existents dels quals no es té referència, etc), intentar resoldre'l.

        **3.** El següent pas, com hem dit, un cop encongit el sistema de fitxers, és encongir "el contenidor" on es troba, és a dir, la partició. Aquest pas és necessari perquè si no el féssim, hi haurà un espai inútil sense poder utilitzar, ja que per una banda no el podríem fer servir per emmagatzemar dades (ja que no estaria formatejat) però per l'altra estaria bloquejat per fer res en ell perquè constaria que aquest espai pertany a una determinada partició ja existent, i el que volem precisament és crear-ne una de nova en aquest espai sense format.
        
        Una de les eines de gestió de particions més coneguda és *fdisk*. No obstant, no té cap opció per redimensionar particions, així que el mètode a utilitzar serà eliminar la partició actual i crear-ne una de nova començant pel mateix lloc que l'antiga amb una mida més petita (englobant tot el sistema de fitxers encongit prèviament, això sí). Si sorprén el fet que eliminar una partició no esborra la informació, hem de tenir present que la informació està emmagatzemada al sistema de fitxers i que les particions només són el mètode per accedir-hi: el que estem fent, doncs, és deixar la informació temporalment inaccessible per tot seguit ubicar-la de nou. Així doncs, el procés serà el següent: primer executarem la comanda *fdisk* sobre el disc on treballarem...:
        
        ```
        # fdisk /dev/vda
        ```

        ...i això ens portarà a un entorn interactiu on haurem d'indicar les opcions adients. Concretament:
        
        * La tecla 'p' (de *'print'*) ens mostra l'estructura de particions del disc
        
        ![](gestio_particions_assets/terminal/3.debian/b.png){style="height:50%;width:50%"} 
        
        * La tecla 'd' de (*'delete'*) ens permetrà eliminar la partició triada a continuació (en el nostre cas, la nº1)
        
        ![](gestio_particions_assets/terminal/3.debian/c.png){style="height:50%;width:50%"} 
        
        * La tecla 'n' de (*'new'*) ens permetrà crear una nova partició, la qual indicarem que volem que sigui de "primària" (sempre que sigui possible serà recomanable que sigui així; de fet ja és l'opció per defecte), que sigui la nº1 de nou, que comenci al mateix sector que començava l'anterior partició nº1 (valor per defecte que se'n proposa) però que acabi, atenció, no al darrer sector disponible (perquè llavors estaríem recreant de nou una partició amb la mateixa mida que l'antiga) sinó que tingui "només" 30G, que és justament l'espai amb el què hem encongit el sistema de fitxers; noteu que per indicar mida en lloc d'un número final de sector cal precedir la xifra pel símbol "+". A la pregunta de si volem esborrar la "signatura" de la partició pretèrita podem contestar que sí. 
        
        * La tecla 'w' de (*'write'*) cal prèmer-la per a què tots aquest canvis es facin efectius al disc
        
        ![](gestio_particions_assets/terminal/3.debian/d.png){style="height:50%;width:50%"} 

        **4.** Un cop ja tenim l'espai buit per poder crear una nova partició, ho farem, de nou utilitzant l'eina *fdisk*, de forma similar al punt anterior:
        
        ```
        # fdisk /dev/vda
        ```

        Ara haurem de prèmer només la tecla 'n' (per crear una nova partició, la qual triarem que torni a ser primària, així que se li assignarà automàticament el nº4 perquè és la última xifra disponible per aquest tipus de particions (només n'hi poden haver quatre i ja n'hi ha tres al disc) i tindrà com a primer i darrer sector del disc el que la pròpia eina ens proposi, ja que seran el primer i darrer sector disponible d'espai buit, respectivament) i la tecla 'w' (per escriure els canvis en disc)
        
        ![](gestio_particions_assets/terminal/3.debian/e.png){style="height:50%;width:50%"}
             
        Podem confirmar, ja des de fora de la consola interactiva interna de l'eina *fdisk*, que, efectivament, tenim una nova partició ("/dev/vda4") amb una mida de prop de 50GB, tal com volíem, executant directament la comanda següent (equivalent a la tecla 'p' dins de la consola interactiva):
        
        ```
        # fdisk -l /dev/vda
        ```

        ![](gestio_particions_assets/terminal/3.debian/f.png){style="height:50%;width:50%"}

        **5.** Finalment, li haurem d'assignar un sistema de fitxers a aquesta nova partició (és a dir, l'haurem de "formatar"). Això ho podem aconseguir fàcilment amb la següent comanda, la qual formatarà la partició indicada concretament en Ext4 (tot i que la pròpia comanda *mkfs* és capaç de formatar en molts altres formats diferents, si així ho desitjem), afegint-hi una etiqueta:
        
        ```
        # mkfs.ext4 -L "Les meves dades" /dev/vda4
        ```
    
    === "Fedora 38 Workstation"
    
        **1.** Observarem primer l'estructura de particions dels dispositius de blocs (discos, CD/DVD...) reconeguts pel sistema mitjançant la següent comanda
       
        ```
        # lsblk
        ```
    
        Tal com es pot veure a la captura següent, corresponent a la sortida de la comanda anterior, a més d'altres dispositius que no ens interessen (com els de tipus "loop" o "zram", per ús intern del sistema "Live", o la pròpia lectora de CD/DVD, "sr0"), en la plantilla utilitzada hi ha un disc anomenat "vda" de 100GB on destaca de molt per la seva mida la partició "vda3". Aquesta serà la partició que encongirem
       
        ![](gestio_particions_assets/terminal/4.fedora/a.png){style="height:50%;width:50%"} 
       
        !!! note "Trobar més informació sobre una partició"

            La comanda *lsblk* ens pot proporcionar molta més informació sobre les particions detectades als discos de la màquina, més enllà de la seva mida i eventual punt de muntatge. Per exemple, ens pot mostrar el sistema de fitxers de cada partició així com també la quantitat d'espai utilitzat i lliure en cadascuna d'elles. Això es pot aconseguir especificant individualment les columnes que es volen mostrar a la seva sortida amb el paràmetre -o (en lloc de quedar-nos amb les columnes que se'ns mostra per defecte). Concretament, la següent comanda mostraria, cada partició o disc, el seu nom, si és partició o disc, la seva mida, els seus eventuals punts de muntatge, el seu sistema de fitxers ("format"), la mida del seu sistema de fitxers (que normalment coincidirà amb la mida de la partició, tal com de seguida explicarem), la quantitat d'espai utilitzat, la quantitat d'espai lliure i el percentatge d'espai utilitzat:
            
            ```
            # lsblk -o NAME,TYPE,SIZE,MOUNTPOINTS,FSTYPE,FSSIZE,FSUSED,FSAVAIL,FSUSE%
            ```

            Una altra comanda que mostra una informació similar a l'anterior és:
            
            ```
            # df -hT
            ```
            
            No obstant, les comandes anteriors mostren el sistema de fitxers i la quantitat d'espai utilitzat només de les particions que estan muntades en aquest moment, de les que no, no. Per tant, en el cas de la nostra pràctica, on el disc de treball no té cap partició muntada, no les podrem fer servir. Afortunadament, tenim alternatives. La següent comanda ens mostra el sistema de fitxers d'una partició (com per exemple, en el nostre cas, "/dev/vda3") sense que aquesta calgui que estigui muntada:
        
            ```
            # file -s /dev/vda3
            ``` 
        
            D'altra banda, la següent comanda mostra la quantitat d'espai utilitzat (vs l'espai total) en particions de tipus Btrfs, com és la "/dev/vda3" on estem treballant en aquest cas (cal tenir en compte que aquesta comanda només funciona en particions formatejades en Btrfs; cada sistema de fitxers vindrà acompanyat de la seva comanda associada adient per obtenir aquest tipus d'informació; haurem de conèixer quina és en cada cas)
        
            ```
            # btrfs filesystem show
            ```
       
        **2.** Un cop reconeguda la partició d'interès com la "/dev/vda3", procedirem a encongir-la. Aquí cal tenir en compte un concepte molt important, que és que no és el mateix la partició en sí (que no és res més que una "caixa" buida reconeguda per Linux amb un inici i un final dins del disc) que el sistema de fitxers que conté (el qual representa la seva estructura interna). Això a la pràctica significa que hem de redimensionar les dues coses: en el cas d'engrandir, primer ho faríem amb la partició i seguidament, per poder fer ús efectiu d'aquest nou espai, ho faríem amb el sistema de fitxers; en el cas d'encongir (que és el cas d'aquesta pràctica), primer encongirem el sistema de fitxers per tal què pugui caber en la nova mida que tindrà la partició  (això ho podrem fer, òbviament, sempre i quan encara hi hagi espai sense utilitzar dins del sistema de fitxers que ens permeti reduir la seva mida sense problema), la qual encongirem a continuació en un segon pas.
        
        Així doncs, encongirem 50GB el sistema de fitxers de "/dev/vda3" amb les següents comandes. A banda de tenir en compte que la comanda *btrfs filesystem resize* només funciona en particions formatejades en Btrfs (en el cas de tenir un altre sistema de fitxers, caldria conèixer quina seria la comanda equivalent), cal saber que aquesta comanda només funciona en particions muntades, així que, tal com es mostra a continuació, haurem de muntar prèviament la partició en qüestió per poder executar-la (i desmuntar-la tot seguit):

        ```
        # mkdir puntdemuntatge
        # mount /dev/vda3 puntdemuntatge
        # btrfs filesystem resize -50G puntdemuntatge
        # umount puntdemuntatge
        # rmdir puntdemuntatge
        ```

        **3.** El següent pas, com hem dit, un cop encongit el sistema de fitxers, és encongir "el contenidor" on es troba, és a dir, la partició. Aquest pas és necessari perquè si no el féssim, hi haurà un espai inútil sense poder utilitzar, ja que per una banda no el podríem fer servir per emmagatzemar dades (ja que no estaria formatejat) però per l'altra estaria bloquejat per fer res en ell perquè constaria que aquest espai pertany a una determinada partició ja existent, i el que volem precisament és crear-ne una de nova en aquest espai sense format.
        
        Una de les eines de gestió de particions més coneguda és *fdisk*. No obstant, no té cap opció per redimensionar particions, així que el mètode a utilitzar serà eliminar la partició actual i crear-ne una de nova començant pel mateix lloc que l'antiga amb una mida més petita (englobant tot el sistema de fitxers encongit prèviament, això sí). Si sorprén el fet que eliminar una partició no esborra la informació, hem de tenir present que la informació està emmagatzemada al sistema de fitxers i que les particions només són el mètode per accedir-hi: el que estem fent, doncs, és deixar la informació temporalment inaccessible per tot seguit ubicar-la de nou. Així doncs, el procés serà el següent: primer executarem la comanda *fdisk* sobre el disc on treballarem...:
        
        ```
        # fdisk /dev/vda
        ```

        ...i això ens portarà a un entorn interactiu on haurem d'indicar les opcions adients. Concretament:
        
        * La tecla 'p' (de *'print'*) ens mostra l'estructura de particions del disc
        
        ![](gestio_particions_assets/terminal/4.fedora/b.png){style="height:50%;width:50%"} 
        
        * La tecla 'd' de (*'delete'*) ens permetrà eliminar la partició triada a continuació (en el nostre cas, la nº3)
        
        ![](gestio_particions_assets/terminal/4.fedora/c.png){style="height:50%;width:50%"} 
        
        * La tecla 'n' de (*'new'*) ens permetrà crear una nova partició, la qual ha de ser la nº3 de nou, ha de començar al mateix sector que començava l'anterior partició nº3 (és a dir, hem de triar el valor per defecte que se'n proposa) però ha d'acabar, atenció, no al darrer sector disponible (perquè llavors estaríem recreant de nou una partició amb la mateixa mida que l'antiga) sinó que ha de tenir "només" 30G, que és justament l'espai amb el què hem encongit el sistema de fitxers; noteu que per indicar mida en lloc d'un número final de sector cal precedir la xifra pel símbol "+". A la pregunta de si volem esborrar la "signatura" de la partició pretèrita podem contestar que sí. 
        
        * La tecla 'w' de (*'write'*) cal prèmer-la per a què tots aquest canvis es facin efectius al disc
        
        ![](gestio_particions_assets/terminal/4.fedora/d.png){style="height:50%;width:50%"} 

        **4.** Un cop ja tenim l'espai buit per poder crear una nova partició, ho farem, de nou utilitzant l'eina *fdisk*, de forma similar al punt anterior:
        
        ```
        # fdisk /dev/vda
        ```

        Ara haurem de prèmer només la tecla 'n' (per crear una nova partició, a la qual se li assignarà automàticament el nº5 perquè és la primera xifra disponible i tindrà com a primer i darrer sector del disc el que la pròpia eina ens proposi, ja que seran el primer i darrer sector disponible d'espai buit, respectivament) i la tecla 'w' (per escriure els canvis en disc)
        
        ![](gestio_particions_assets/terminal/4.fedora/e.png){style="height:50%;width:50%"}
             
        Podem confirmar, ja des de fora de la consola interactiva interna de l'eina *fdisk*, que, efectivament, tenim una nova partició ("/dev/vda5") amb una mida de prop de 50GB, tal com volíem, executant directament la comanda següent (equivalent a la tecla 'p' dins de la consola interactiva):
        
        ```
        # fdisk -l /dev/vda
        ```

        ![](gestio_particions_assets/terminal/4.fedora/f.png){style="height:50%;width:50%"}

        **5.** Finalment, li haurem d'assignar un sistema de fitxers a aquesta nova partició (és a dir, l'haurem de "formatar"). Això ho podem aconseguir fàcilment amb la següent comanda, la qual formatarà la partició indicada concretament en Ext4 (tot i que la pròpia comanda *mkfs* és capaç de formatar en molts altres formats diferents, si així ho desitjem), afegint-hi una etiqueta:
        
        ```
        # mkfs.ext4 -L "Les meves dades" /dev/vda5
        ```


### Muntatge de particions 

Un cop ja tenim la nova partició creada i formatejada, ens falta el darrer pas: poder-hi tenir accés per tal de començar llavors a poder gestionar el seu contingut. O dit d'una altra manera, hem de muntar-la. Però això ja no ho farem des del entorn "Live" sinò des del sistema instal·lat en el propi disc de la màquina. És per això que el que farem serà:

**1.** Apagar l'escriptori

**2.** Anar al panell d'edició de l'escriptori (tal com s'explica [aquí](https://isard.gitlab.io/isardvdi-docs/user/edit_desktop)) i allà fer dues coses: treure l'arxiu Iso com a mitjà i tornar a establir l'ordre d'arranc a "Hard disk"

**3.** Iniciar l'escriptori 


#### Muntatge manual de la nova partició

Un cop iniciada sessió dins del sistema arrencat, si aquest és de tipus Server ja estarem dins del terminal, que és el que necessitem; si estem dins d'una sessió gràfica de l'entorn Gnome, haurem d'iniciar un terminal tal com s'ha explicat en els passos anteriors. En qualsevol cas, per muntar la nova partició recentment formatejada en Ext4 caldrà realitzar els següents passos:

**1.** Crear una carpeta, la qual representarà el punt de muntatge de la partició en qüestió. Aquesta carpeta pot estar situada en qualsevol lloc, però la crearem sota "/mnt", una ubicació específicament pensada per allotjar de forma centralitzada els diversos eventuals punts de muntatge que vulguem crear:

```
$ sudo mkdir /mnt/punt
```

**2.** Realitzarem el muntatge pròpiament dit (segons hem realitzat els passos anteriors, en els escriptoris basats en les plantilles d'Ubuntu i Fedora la partició a muntar és "/dev/vda5", tal com es mostra a la comanda següent, però en els escriptoris basats en la plantilla de Debian cal tenir en compte que aquesta serà en canvi "/dev/vda4"):

```
$ sudo mount /dev/vda5 /mnt/punt
```

Tot seguit, en la majoria de cops serà convenient executar també la següent comanda per poder treballar amb el contingut de dins del punt de muntatge des de l'usuari actual (és a dir, sense haver de convertir-nos en administrador) ja que com que la comanda *mount* s'executa com a "root", si no fem res aquest seria l'usuari propietari del contingut del punt de muntatge i això fa que calgués treballar-hi sempre amb aquest usuari:

```
$ sudo chown -r $USER:$USER /mnt/punt
``` 

**3.** Es pot comprovar que, efectivament, el muntatge s'ha realitzat correctament observant la sortida d'alguna de les següents comandes:

* Per veure totes les particions Ext4 muntades, i en quin punt de muntatge:

```
$ findmnt -t ext4 
```

* Per veure si hi ha alguna partició muntada en "/mnt/punt", i en seu format

```
$ findmnt -T /mnt/punt
```

* Per veure si la partició indicada està muntada, en quin punt de muntatge i quin sistema de fitxers té:

```
$ findmnt -S /dev/vda5
```

**4.** Ja podrem treballar dins de "/mnt/punt" com una carpeta qualsevol més, copiant-hi fitxers i subcarpetes, movent-los, editant-los, eliminant-los, etc

**5.** Opcionalment, si ja no utilitzarem més el seu contingut, podem desmuntar la partició amb la següent comanda. 

``` 
$ umount /mnt/punt
```

En tot cas, quan el sistema s'apagui i es torni a reiniciar, el punt de muntatge tornarà a estar desmuntat, tot i no haver executat anterior: els muntatges són per natura efímer. Per tant, haurem de repetir els pas nº2 de nou per tornar a poder començar a treballar amb el seu contingut


#### Muntatge automàtic de la nova partició en arrencar el sistema

Si no volem haver de muntar manualment una determinada partició en un determinat punt de muntatge cada cop que el sistema s'inicïi, existeix la possibilitat de fer aquesta acció de forma automàtica durant l'arranc del sistema. Concretament, ho podem fer de vàries maneres, tal com es mostra a continuació.

!!! example "Mètodes per automatitzar els muntatges de particions"
 
    === "Mitjançant l'edició de l'arxiu '*/etc/fstab*' (mètode clàssic)"

        Cada línia d'aquest arxiu consta de sis camps separats per un (o més) espais en blanc o per un tabulador, tant se val (per més informació, consulteu *man fstab*):

        * El 1r camp començant per l'esquerra indica la partició que serà muntada durant l'arranc del sistema. Es pot indicar el seu nom directament (per exemple, "/dev/vda4") però també la seva etiqueta (així: LABEL="La meva etiqueta") o també el seu UUID (així, UUID="xxxx-xxx...")

        * El 2n camp indica la ruta del seu punt de muntatge 

        * El 3r camp indica el sistema de fitxers en què està formatada aquesta partició (valors possibles són: "ext4", "xfs", "vfat" -per “FAT32”-, "ntfs" o "auto", entre altres; el valor "auto" serveix per no especificar cap sistema en concret i què sigui el sistema qui el determini ell sol en el moment de fer el muntatge)
        
        * El 4r camp indica, separades per comes, les opcions de muntatge s'hi apliquen. Algunes de les més habituals es resumeixen a continuació, però per més detall es recomanda consultar *man mount* així com també *man ext4* (en el cas de les particiones Ext4):
        
              * **ro** :  Munta en mode de només lectura. Per defecte les muntatges es fan en mode escriptura (**rw**).

              *  **auto** :  Indica que la partició en qüestió es muntarà automàticament durant l'arranc. En altres paraules: és la manera "d'activar" aquesta línia. L'opció contrària és **noauto**, que serveix per "desactivar" la línia sense haver d'esborrar-la (fent, per tant, que només es pugui montar la partició de forma manual amb la comanda *mount*)

              * **exec** : Permet que es puguin executar els binaris existents a la partició en qüestió. L'opció contrària és **noexec**
        
              *  **sync** : Fa que les escriptures a la partició en qüestió es realitzin de forma síncrona (és a dir, que s'apliquin immediatament). L'opció contrària és **async**, la qual implica que les escriptures es realitzin de forma asíncrona (és a dir, que totes les escriptures s'apliquin de cop passat un temps determinat). 

              *  **defaults** : Equival, entre altres, a les opcions "rw", "auto", "exec" i "async"

        * El 5è camp actualment no s'utilitza i quasi sempre val 0

        * El 6è camp indica si (i quan) la comanda *fsck* s'executarà quan es munti automàticament aquesta partició. Un valor 0 vol dir que no es comprova el sistema de fitxers, un valor 1 vol dir que sí i un valor 2 vol dir que sí, però després d'haver comprovat les particions marcades amb valor 1 (normalment la partició arrel del sistema sol tenir un 1 mentre la resta de particions pot tenir 0 o 2).

        Així doncs, en el nostre cas, la línia que hauríem d'afegir hauria de ser similar a la següent: 

        ```unixconfig
        /dev/vda4  /mnt/punt  ext4  defaults  0  0 	
        ``` 

        Un cop gravada una línia com l'anterior en l'arxiu "/etc/fstab", només caldrà reiniciar el sistema per tal què la partició en qüestió es munti automàticament (o, alternativament, executar la següent comanda per a què el muntatge s'efectuï de forma immediata):
        
        ```
        $ sudo mount -a
        ```
 
    === "Mitjançant l'edició d'una *unit* Systemd de tipus *mount*"

        En lloc d'utilitzar l'arxiu "/etc/fstab", una alternativa és fer ús de una de les múltiples funcionalitats que aporta el subsistema Systemd, present a la gran majoria de sistemes Linux actuals. En concret, la funcionalitat de justament muntar automàticament particions durant l'arranc del sistema. 
        
        Per aconseguir això, només cal crear dins de la carpeta "*/etc/systemd/system*" un arxiu amb extensió "*.mount*" el nom del qual sigui obligatòriament la ruta absoluta del punt de muntatge associat però eliminant la primera barra ("/"), corresponent a la carpeta arrel, i substituïnt la resta d'eventuals barres que apareguin a la ruta per guions ("-"). Per exemple, en el nostre cas, si el punt de muntatge és "/mnt/punt", aquest arxiu de configuració haurà de ser llavors "**/etc/systemd/system/mnt-punt.mount**" 

        L'interior d'aquest arxiu de configuració (el qual manté la mateixa estructura els fitxers de configuració de qualssevol altre tipus d'"units" Systemd, com ara els serveis, els "targets", etc) haurà de ser similar al següent:
        
        ```ini
        [Unit]
        Description=Fitxer de configuració del punt de muntatge "/mnt/punt" sobre la partició "/dev/vda4", formatada en Ext4 i utilitzant les opcions per defecte
        [Mount]  
        What=/dev/vda4
        Where=/mnt/punt
        Type=ext4
        Options=defaults     
        [Install]
        WantedBy=multi-user.target
        ``` 
        
        Un cop gravat l'arxiu anterior, per realitzar el muntatge immediatament es pot executar la comanda següent:
        
        ```
        $ sudo systemctl start mnt-punt.mount
        ```

        Però si volem que es faci automàticament a cada arrencada de forma automàtica, el que haurem d'executar és la comanda següent:

        ```        
        $ sudo systemctl enable mnt-punt.mount
        ```  

        En tot cas, sempre podrem saber si aquest punt de muntatge està actiu en un moment donat executant la comanda següent:

        ```
        $ systemctl status mnt-punt.mount
        ```
 
    === "Mitjançant Gnome Disks (en el cas d'iniciar un sistema amb entorn Gnome)"

        La mateixa aplicació Gnome Disks que hem comentat en apartats anteriors també ens pot servir per definir que volem muntar una determinada partició de forma permanent. Concretament, si triem la partició en qüestió, pitjem sobre el botó amb el símbol de les rodes dentades i triem l'opció "Edita les opcions de muntatge", apareixerà un quadre com el següent, on podrem especificar, tal com es veu, la ruta desitjada pel punt de muntatge de la partició sel·leccionada, les opcions de muntatge desitjades, així com indicar el seu sistema de fitxers
        
        ![](gestio_particions_assets/zmuntatgegdisks.png){style="height:50%;width:50%"}
        

## Correcció de la pràctica

Per confirmar si l'alumne ha realitzat tots els passos d'aquesta pràctica correctament, es suggereixen els següents ítems per comprovar-ho:

- [x] Que la sortida de les comandes *lsblk* o *fdisk -l* mostrin l'existència d'una determinada partició amb una determinada mida
- [x] Que la sortida de la comanda *file -s ...* mostri un determinat sistema de fitxers per una determinada partició donada
- [x] Que la sortida de la comanda *findmnt -t ext4* mostri l'existència d'una determinada partició i el seu corresponent punt de muntatge 
- [x] Que la sortida de la comanda *findmnt -T ...* mostri una determinada partició pel punt de muntatge donat
- [x] Que la sortida de la comanda *findmnt -S ...* mostri un determinat punt de muntatge per la partició donada
- [x] Que l'arxiu "/etc/fstab" contingui una determinada línia o bé que existeixi un determinat arxiu ".mount" dins de la carpeta "/etc/systemd/system" amb un determinat contingut
- [x] Que es comprovi l'estat mostrat per la comanda *systemctl status ...* per un arxiu ".mount" donat

Per automatitzar les comprovacions anteriors, es suggereix l'ús de l'eina de correccions desateses [Teuton](https://github.com/teuton-software/teuton) 

