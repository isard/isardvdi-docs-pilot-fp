# Posada en marxa del servidor DHCP Kea

![Imatge Kea](servidor_dhcp_kea_assets/logokea.png){style="height:25%;width:25%" align=left} 

L'objectiu d'aquesta pràctica és implementar un servidor DHCP (concretament el [servidor Kea](https://www.isc.org/kea)) en una màquina virtual per tal que pugui assignar configuracions de xarxa a altres màquines virtuals clients pertanyents a la mateixa xarxa

Durant molts anys el servidor DHCP "canònic" utilitzat a la gran majoria de sistemes Linux ha estat l'[ISC DHCP Server](https://www.isc.org/dhcp) però des del 2022 ha arribat al seu final de vida i es recomana el seu reemplaçament per Kea, un servidor DHCP (i també DDNS) modern per IPv4 i IPv6 desenvolupat per la mateixa organització ISC, totalment gestionable via REST API i capaç d'emmagatzemar prèstecs i reserves en SGBDs com MySQL o PostgreSQL, entre altres funcionalitats avançades.

## Cicles, Mòduls i UFs on es pot realitzar aquesta pràctica

* **SMX  MP07** UF1:Configuració de la xarxa (DNS i DHCP)
* **ASIX MP08** UF1:Serveis de noms i configuració automàtica

## Infraestructura prèvia necessària per implementar aquesta pràctica

!!! warning "Vigila l'ús de vCPUs i de RAM!"

    És **molt important** que la quantitat de vCPUs i de memòria RAM assignades a les màquines utilitzades en aquesta pràctica sigui el més adjustada possible!!
    
    La quantitat global d'hores que un centre educatiu té disponible per utilitzar IsardVDI (les anomenades "DHN", de "Desktop Hours Number") ve donada per la següent [fòrmula](https://isard.gitlab.io/isardvdi-docs-pilot-fp/usage/usage.ca/#__tabbed_1_1). Això implica que quanta més vCPUs i -sobre tot- RAM s'assignin a les màquines creades dins d'un centre concret, menys hores d'Isard aquest centre podrà consumir en total.

!!! info "Idees per no consumir gaire RAM"

    * Utilitzar distribucions basades en Linux de tipus "Server" (és a dir, sense entorn gràfic). Exemples de distribucions d'aquest tipus són, per exemple, [Debian Server](https://www.debian.org), [Ubuntu Server](https://ubuntu.com/download/server), [Fedora Server](https://fedoraproject.org/server), [openSUSE Server](https://get.opensuse.org/server), [Rocky Linux Server](https://rockylinux.org) o [Alma Linux Server](https://almalinux.org), entre moltes altres. 
    
    * Utilitzar distribucions que, tot i tenir un entorn gràfic, aquest sigui més minimalista que els entorns estàndard (com GNOME o KDE) i, per tant, més lleuger de recursos. Exemples de distribucions d'aquest tipus són: [Slitaz](https://www.slitaz.org), [Puppy](https://puppylinux-woof-ce.github.io), [Slax](https://www.slax.org), [Bodhi](https://www.bodhilinux.com), [Lubuntu](https://lubuntu.me), [Fedora LXQT](https://fedoraproject.org/spins/lxqt) o fins i tot [Fedora Sway](https://fedoraproject.org/spins/sway), entre moltes altres
       
    En tot cas, si a l'hora de voler crear un escriptori s'optés per triar alguna de les distribucions anteriors, caldria comprovar primer que IsardVDI ofereix la plantilla-base corresponent; si no és així, llavors caldria realitzar manualment la instal·lació de l'escriptori desitjat a partir del medi ISO adient (tal com s'explica [aquí](https://isard.gitlab.io/isardvdi-docs/advanced/media)). 
    
    Si aquest sistema instal·lat es volgués utilitzar, a més, per fer un desplegament, llavors caldria primer crear a partir de l'escriptori anterior una plantilla pròpia (tal com s'explica [aquí](https://isard.gitlab.io/isardvdi-docs/advanced/templates/#create)) i tot seguit fer el desplegament a partir d'ella (tal com s'explica [aquí](https://isard.gitlab.io/isardvdi-docs/advanced/deployments)).

!!! tip "Utilitza el visor SPICE per millorar la teva experiència d'usuari"

    A l'hora de triar un visor per a les teves màquines, et recomanem que utilitzis el visor SPICE ja que ofereix, a més de molt poca latència, una major funcionalitat i flexibilitat que els altres visors disponibles. Per exemple, mitjançant aquest visor podràs gaudir de l'àudio integrat i seràs capaç de copiar-pegar entre la teva màquina real local i les màquines Isard (si no són de tipus "Server"), així com també connectar-hi dispositius USB en la primera i que siguin visibles en les segones, entre altres possibilitats. Per saber més informació sobre les característiques dels diferents visors disponibles, pots consultar [el manual](https://isard.gitlab.io/isardvdi-docs/user/viewers/viewers)


Per realitzar aquesta pràctica es necessitarà disposar com a mínim de dues màquines virtuals: una farà de servidor DHCP i l'altra farà de client DHCP (ja sigui de forma automàtica en arrencar el sistema o bé mitjançant l'execució explícita de les comandes adients).

Aquestes dues màquines poden consistir en dos desplegaments oferts pel professor a tot el grup-clase o bé dos escriptoris generats per cada alumne individualment, tant se val. A continuació es detallen els passos a fer en qualsevol d'aquestes dues opcions.

!!! example "Creació de la màquina servidora DHCP"

    === "En forma de desplegament"
    
        1. Triar l'opció "Desplegaments del menú superior i tot seguit pitjar en el botó "Desplegament nou"
        2. Triar, al formulari web que apareix, les característiques que desitjem que tingui la màquina. En destacarem les que són rellevants per aquesta pràctica:
            + **Visible**: SÍ (caldrà indicar a més, en l'apartat corresponent, el grup i/o alumnes concrets als quals se'ls vol mostrar la màquina)
            + **vCPUs** i **RAM**: És important tenir en compte el que s'indica al primer quadre d'aquesta pàgina  
            + **Plantilla base**: Qualsevol de tipus Linux (Debian, Ubuntu, Fedora). És important tenir en compte, però, el que s'indica al segon quadre d'aquesta pàgina
            + **Visors**: És important tenir en compte el que s'indica al tercer quadre d'aquesta pàgina  
            + **Xarxes**: Calen dues: la "Default" (per tenir accés a la descàrrega de paquets de l'exterior) i "Personal1" (la interfície on el servidor escoltarà les peticions) dels clients)     
        3. Pitjar el botó "Crear". 
        
        En tot cas, teniu més informació sobre com crear desplegaments i les possibilitats que aquests ofereixen [aquí](https://isard.gitlab.io/isardvdi-docs/advanced/deployments)
    
    === "En forma d'escriptori"
    
        1. Pitjar en el botó "Escriptori nou"
        2. Triar, al formulari web que apareix, les característiques que desitjem que tingui la màquina. En destacarem les que són rellevants per aquesta pràctica:
            + **vCPUs** i **RAM**: És important tenir en compte el que s'indica al primer quadre d'aquesta pàgina  
            + **Plantilla base**: Qualsevol de tipus Linux (Debian, Ubuntu, Fedora). És important tenir en compte, però, el que s'indica al segon quadre d'aquesta pàgina
            + **Visors**: És important tenir en compte el que s'indica al tercer quadre d'aquesta pàgina  
            + **Xarxes**: Calen dues: la "Default" (per tenir accés a la descàrrega de paquets de l'exterior) i "Personal1" (la interfície on el servidor escoltarà les peticions) dels clients)     
        3. Pitjar el botó "Crear".     
        
        En tot cas, teniu més informació sobre com crear escriptoris i les possibilitats que aquests ofereixen [aquí](https://isard.gitlab.io/isardvdi-docs/user/create_desktop)


!!! example "Creació de la màquina client DHCP"

    === "En forma de desplegament"
    
        1. Triar l'opció "Desplegaments del menú superior i tot seguit pitjar en el botó "Desplegament nou"
        2. Triar, al formulari web que apareix, les característiques que desitjem que tingui la màquina. En destacarem les que són rellevants per aquesta pràctica:
            + **Visible**: SÍ (caldrà indicar a més, en l'apartat corresponent, el grup i/o alumnes concrets als quals se'ls vol mostrar la màquina)
            + **vCPUs** i **RAM**: És important tenir en compte el que s'indica al primer quadre d'aquesta pàgina  
            + **Plantilla base**: Qualsevol de tipus Linux (Debian, Ubuntu, Fedora). És important tenir en compte, però, el que s'indica al segon quadre d'aquesta pàgina
            + **Visors**: És important tenir en compte el que s'indica al tercer quadre d'aquesta pàgina  
            + **Xarxes**: Només en cal una: la "Personal1", per tal d'ubicar aquesta interfície a la mateixa xarxa on es trobarà el servidor escoltant les eventuals peticions. El no tenir xarxa "Default" farà que inicialment aquesta màquina no tingui accés a Internet, però durant la pràctica aquest inconvenient se solucionarà.
        3. Pitjar el botó "Crear".
        
        En tot cas, teniu més informació sobre com crear desplegaments i les possibilitats que aquests ofereixen [aquí](https://isard.gitlab.io/isardvdi-docs/advanced/deployments)
    
    === "En forma d'escriptori"
    
        1. Pitjar en el botó "Escriptori nou"
        2. Triar, al formulari web que apareix, les característiques que desitjem que tingui la màquina. En destacarem les que són rellevants per aquesta pràctica:
            + **vCPUs** i **RAM**: És important tenir en compte el que s'indica al primer quadre d'aquesta pàgina  
            + **Plantilla base**: Qualsevol de tipus Linux (Debian, Ubuntu, Fedora). És important tenir en compte, però, el que s'indica al segon quadre d'aquesta pàgina
            + **Visors**: És important tenir en compte el que s'indica al tercer quadre d'aquesta pàgina  
            + **Xarxes**: Només en cal una: la "Personal1", per tal d'ubicar aquesta interfície a la mateixa xarxa on es trobarà el servidor escoltant les eventuals peticions. El no tenir xarxa "Default" farà que inicialment aquesta màquina no tingui accés a Internet, però durant la pràctica aquest inconvenient se solucionarà
        3. Pitjar el botó "Crear".
        
        En tot cas, teniu més informació sobre com crear escriptoris i les possibilitats que aquests ofereixen [aquí](https://isard.gitlab.io/isardvdi-docs/user/create_desktop)


## Enunciat de la pràctica

### Configuració del servidor DHCP

#### Configuració de xarxa

El primer que hem de fer és assegurar-nos que la interfície de xarxa que donarà el servei DHCP (és a dir, la interfície de xarxa "Personal1") tingui configurada una adreça IP fixa. 

!!! info "Sobre el nom de la interfície de xarxa"

    En aquest document assumirem que aquesta interfície de xarxa "Personal1" del servidor DHCP és reconeguda pel seu sistema amb el nom de *enp2s0* En tot cas, recordeu que aquest nom dependrà de l'ordre en què s'hagin indicat les diferents tarjetes de la màquina en el formulari de creació de desplegament/escriptori 

Assignar una IP fixa es pot fer de moltes formes diferents segons estiguem utilitzant o bé Netplan o bé el servei NetworkManager, o bé el servei Systemd-Networkd, etc. Segons la distribució basada en Linux que haguem triat per implementar el nostre servidor DHCP, estarem en una situació o una altra. És per això que a continuació es mostren les diferents possibilitats existents en els sistemes dels quals IsardVDI ofereix una plantilla predeterminada. Sigui quin sigui el mètode emprat, en tot cas, a aquesta interfície de xarxa *enp2s0* sempre li assignarem la mateixa adreça, la **192.168.1.1/24**

!!! example "Mètodes d'assignació d'una adreça IP fixa"

    === "Ubuntu Desktop"
    
        Aquesta distribució utilitza el mètode *Netplan* per configurar les interfícies de xarxa. En aquest cas, cal editar (com administrador) el fitxer **"/etc/netplan/01-network-manager-all.yaml"** per tal que tingui el següent contingut (compte amb els espais, han de respectar-se tal com apareixen!)...:
        
        ```yaml
        network:
           version: 2
           renderer: NetworkManager
           ethernets:
              enp2s0:
                 addresses: [192.168.1.1/24]
        ``` 
    
        ...i tot seguit executar la comanda següent:
        
        ```bash
        sudo netplan apply
        ```
        
        Aquest mètode només és vàlid en sistemes Ubuntu (tant en l'edició "Desktop" com "Server" (en aquesta darrera, però, el fitxer a editar es diu **"/etc/netplan/00-installer-config.yaml"**). Podeu saber-ne més si consulteu la seva documentació oficial [aquí](https://netplan.readthedocs.io)
    
    === "Debian Server"
    
        Aquesta distribució utilitza el servei *Systemd-Networkd* per configurar les interfícies de xarxa. En aquest cas, cal editar el fitxer *"/etc/systemd/network/enp2s0.network"* per tal que tingui el següent contingut (compte amb les majúscules, han de respectar-se tal com apareixen!)...:
        
        ```ini
        [Match]
        Name=enp2s0
        [Network]
        Address=192.168.1.1/24
        ```
    
        ...i tot seguit executar la comanda següent:
        
        ``` 
        sudo networkctl reload
        ```
        
        Aquest mètode és vàlid en qualsevol sistema on estigui instal·lat i funcionant el servei Systemd-Networkd. La manera de comprovar que sigui així és observant la sortida de la següent comanda:
        
        ```
        systemctl status systemd-networkd
        ```
       
    === "Fedora WorkStation"
    
        Aquesta distribució utilitza el servei **NetworkManager** per configurar les interfícies de xarxa. En aquest cas, cal executar primer la següent comanda...:
        
        ```
        nmcli connection show
        ```
        
        ... per comprovar si la interfície de xarxa *enp2s0* ja està associada a una connexió; si és així, després d'observar el nom d'aquesta connexió, cal eliminar-la amb la següent comanda:
        
        ```
        nmcli connection del *nomConnexioActual* 
        ```
        
        Tot seguit, cal executar la següent comanda:
        
        ```
        nmcli connection add type ethernet ifname enp2s0 con-name *nomConnexioNova* ip4 192.168.1.1/24
        ```
    
        Aquest mètode és vàlid en qualsevol sistema on estigui instal·lat i funcionant el servei NetworkManager. La manera de comprovar que sigui així és observant la sortida de la següent comanda:
        
        ```
        systemctl status NetworkManager
        ```
    
    === "Mètode genèric però temporal ('Iproute2')"
    
        Aquest mètode serveix per qualsevol distribució basada en Linux. Permet assignar l'adreça IP de forma immediata però, no obstant, de forma temporal (fins que el sistema es reinicïi). Consisteix en executar la comanda següent:
        
        ```
        sudo ip address add 192.168.1.1/24 dev enp2s0
        ```

En tot cas, pots comprovar que l'assignació s'ha realitzat correctament observant la sortida de la següent comanda:

```
ip -c address show
```

#### Instal·lació del *software* servidor

A continuació instal·larem el paquet corresponent. Per fer això, obrirem un terminal i executarem el següent:

!!! example "Instal·lació del paquet 'kea' "

    === "Ubuntu"
    
        Executa la següent comanda:
        
        ```
        sudo apt install kea
        ```
    
    === "Debian"
    
        Executa la següent comanda:
        
        ```
        sudo apt install kea
        ```
    
    === "Fedora"
    
        Executa la següent comanda:
        
        ```
        sudo dnf install kea
        ```

!!! info "Sobre el nom dels paquets disponibles a Ubuntu/Debian"

    El paquet "kea" indicat al quadre anterior instal·la "tot" el servidor Kea; és a dir, el servidor DHCPv4, el servidor DHCPv6 i el servidor DDNS (a més d'un servei de control anomenat "kea-ctrl-agent", que serveix per poder configurar dinàmicament els servidors anteriors, i un conjunt d'utilitats d'administració i monitorització, com ara les comandes *kea-admin* o *perfdhcp*, entre altres). No obstant, si només volguéssim implementar el servidor DHCPv4 (com és el cas d'aquesta pràctica), a Ubuntu i Debian existeix la possibilitat d'instal·lar només el paquet específic que aporta aquesta funcionalitat concreta i res més, anomenat **"kea-dhcp4-server"** (les altres funcionalitats també venen empaquetades en sengles paquets separats, anomenats "kea-dhcp6-server", "kea-dhcp-ddns-server", "kea-ctrl-agent" i "kea-admin", respectivament). 


#### Configuració del *software* servidor

Els fitxers de configuració de Kea utilitzen un format JSON però es permeten elements fora del format "canònic", com són les comes finals, diferents tipus de comentaris (#, //, /* */) i la inclusió de fitxers mitjançant la construcció *<?include "file.json"?>*

L'arxiu de configuració del servidor DHCPv4 de Kea és **"/etc/kea/kea-dhcp4.conf"** Una exemple de configuració bàsica que allà hi podria aparéixer escrita és el següent:

```json title="Example de fitxer de configuració bàsic d'un servidor DHCPv4 Kea"
{
    "Dhcp4": {
        "interfaces-config": {
              "interfaces": [ "enp2s0" ]
        },
        "valid-lifetime": 4000,
        "renew-timer": 1000,
        "rebind-timer": 2000,
        "reservations-out-of-pool": true,
        "subnet4": [{ 
            "subnet": "192.168.1.0/24",
            "match-client-id": false,
            "pools": [  
                { "pool": "192.168.1.100-192.168.1.199" }
            ],
            "option-data": [
                { "name": "routers",
                  "data": "192.168.1.1" },
                { "name": "domain-name-servers",
                  "data": "9.9.9.9" },
                { "name": "domain-name",
                  "data": "domini-1.test" }
            ],
            "reservations": [
                { "hw-address": "52:54:00:5c:eb:99",
                  "ip-address": "192.168.1.11" },
                { "hw-address": "52:54:00:2d:8d:a2",
                  "ip-address": "192.168.1.12" }
            ]
        }]
    }
}
```

Els camps que apareixen a l'exemple de configuració anterior són:

* Camp **Dhcp4** : Configuració del servei DHCPv4. A la configuració d'interfícies cal enumerar els adaptadors de xarxa que utilitzarà el servidor.

* Camps **valid-lifetime**, **renew-timer**, **rebind-timer** :  Temps per a l'adquisició i la renovació de concessions.

* Camp **reservations-out-of-pool** :  Quan aquesta opció està activada, el servidor entendrà que per a les reserves (veure camp *"reservations"*) s'utilitzaran adreces que estan fora dels pools establerts (veure camp *"pools"*)

* Camp **subnet4** :  Array dedicat a definir subxarxes. En aquest cas es defineix la subxarxa 192.168.1.0/24.

* Camp **match-client-id** : Aquesta opció permet indicar com ha d'identificar el servidor als clients. Pot ser l'adreça MAC del client (valor false) o el seu DUID, que és un codi identificador del client independent de la interfície de xarxa que utilitzi (valor true)

* Camp **option-data** : Opcions passades al client amb la concessió de xarxa. Típicament: porta d'enllaç, servidors DNS i domini.

* Camp **pools** : Array de blocs d'adreces per assignar als ordinadors que sol·licitin una concessió de xarxa.

* Camp **reservations** : Array amb les reserves (ordinadors coneguts als quals se'ls assigna una adreça determinada). A l'exemple només s'utilitzen les opcions "hw-address" (per indicar la MAC de l'ordinador) i "ip-address" per indicar la IP assignada. Però és possible aportar qualsevol opció de configuració en fer la reserva.

En tot cas, si a partir d'aquí es vol saber més sobre aquest programa, es pot consultar la documentació de [Kea](https://kea.readthedocs.io/en/latest), que és molt completa. També es recomana llegir els comentaris presents en l'arxiu de configuració *"kea-dhcp4.conf"* que ve de sèrie amb la instal·lació estàndard del programa, ja que són molt aclaridors.

#### Posada en marxa del servidor Kea

El paquet Kea proporciona 4 serveis independents (cadascun controlats per una unitat de Systemd particular): 

* El servei *kea-dhcp4-server* és el servidor DHCP IPv4. El seu fitxer de configuració, tal com ja hem dit, és "/etc/kea/kea-dhcp4.conf" 
* El servei *kea-dhcp6-server* és el servidor DHCP IPv6. El seu fitxer de configuració és "/etc/kea/kea-dhcp6.conf" 
* El servei *kea-dhcp-ddns-server* és el servidor DDNS (DNS dinàmic). El seu fitser de configuració és "/etc/kea/kea-dhcp-ddns.conf"
* El servei *kea-ctrl-agent* és un agent de control de Kea que proporciona una interfícia REST (via HTTP o HTTPS) per configurar-lo dinàmicament. El seu fitxer de configuració és "/etc/kea/kea-ctrl-agent.conf"

!!! info "Altres executables i scripts addicionals del projecte Kea"

    * Comanda *kea-admin* : Shell script que permet gestionar internament la base de dades de prèstecs i reserves del servidor DHCP (sigui quin sigui el "backend" utilitzat (memòria, SGBD MySQL o SGBD PostgreSQL). 

    * Comanda *perfdhcp* : Executable que permet mesurar el rendiment i comportament del servidor DHCP mitjançant la generació de gran qüantitat de peticions DHCP provinents d'hipotètics clients falsos.
    
    * Comanda *kea-shell* : Script Python que funciona com a client REST del servei *kea-ctrl-agent*. És a dir, des d'aquest client es poden llençar ordres internes (bé interactivament bé com a paràmetres de la línia de comandes) al servei *kea-ctrl-agent* per tal de controlar, a través seu, el servidor Kea (ja sigui localment, si el servidor Kea funciona a la mateixa màquina que *kea-shell* com, sobre tot, remotament). D'aquesta manera, es disposa d'un mecanisme que permet, per exemple, la reconfiguració en líniea sense necessitat d'aturar el servidor..
 
    * Comanda *keactrl* : Shell script que permet controlar l'inici, l'aturada i la reconfiguració del servidor Kea
  
    Per més informació, es pot consultar la documentació oficial del projecte, disponible [aquí](https://kea.readthedocs.io)

En aquesta pràctica només estarem interessats en el servidor DHCPv4, així que només voldrem posar en marxa el servei *kea-dhcp4-server* i deshabilitar la resta. Tot això ho podem fer executant les següents comandes un sol cop (en els propers reinicis del sistema el servei *kea-dhcp4-server* , i només ell, ja s'haurà d'iniciar automàticament):

```bash
sudo systemctl --now disable kea-dhcp6-server kea-dhcp-ddns-server kea-ctrl-agent
sudo systemctl --now enable kea-dhcp4-server
```




#### Persistència en les concesions

Quan un servidor DHCP assigna una concessió a un equip, heu de desar aquesta informació en algun lloc de manera que persisteixi a possibles reinicis del servidor. D'aquesta manera, quan el servidor arrenca pot consultar aquesta informació per saber quines concessions continuen sent vàlides i evitar assignar aquestes adreces a altres equips.

Kea pot utilitzar tres "backends" per desar aquesta informació: "memfile", "MySQL" i "PostgreSQL". El backend "memfile" és l'utilitzat per defecte i fa servir un fitxer en format .csv per desar informació sobre les concessions. Típicament aquest fitxer és **"/var/lib/kea/kea-leases4.csv"** i té el següent aspecte:

```unixconfig
address,hwaddr,client_id,valid_lifetime,expire,subnet_id,fqdn_fwd,fqdn_rev,hostname,state,user_context
192.168.241.75,e0:63:da:a6:ef:cf,,600,1673032745,18,0,0,unifi-p1a1,0,
192.168.241.92,70:a7:41:64:12:49,,600,1673032749,18,0,0,unifi-u6-pro,0,
192.168.21.51,e4:e7:49:a4:2f:20,,600,1673032760,11,0,0,hp-laserjet-color-m452dn-secretària.,0,
```

A aquest fitxer s'hi afegeix una línia cada vegada que es produeix una concessió (el temps d'expiració està indicat en format UNIX). Tal com hem dit, durant l'arrencada Kea el llegeix del tot per veure si hi ha alguna concessió vigent. Per defecte, cada hora aquest fitxer és rotat per evitar que creixi de forma indefinida


### Configuració dels clients DHCP

A continuació es presenten diversos mètodes per, des d'una màquina client, demanar al nostre servidor DHCP recentment configurat les dades de xarxa disponibles (adreça IP, porta d'enllaç, servidor DNS i domini). També s'indicarà com aconseguir que les peticions DHCP es realitzin per la màquina client de forma automàtica en el moment de l'arranc del sistema (per tal de no haver-ho de fer manualment). 

!!! info "Sobre el nom de la interfície de xarxa"

    En aquest document assumirem que la única interfície de xarxa del client DHCP és reconeguda pel seu sistema amb el nom de *enp1s0* En aquest cas és la única interfície existent a la màquina però recordeu que, si n'hi hagués més, aquest nom dependrà de l'ordre en què s'hagin indicat les diferents tarjetes de la màquina en el formulari de creació de desplegament/escriptori 

!!! example "Mètodes d'assignació de dades de xarxa via DHCP"

    === "Ubuntu Desktop"
    
        Edita (com administrador) el fitxer **"/etc/netplan/01-network-manager-all.yaml"** per tal que tingui el següent contingut (compte amb els espais, han de respectar-se tal com apareixen!)...:
        
        ```yaml
        network:
           version: 2
           renderer: NetworkManager
           ethernets:
              enp1s0:
                 dhcp4: yes
        ```
    
        ...i tot seguit executa la següent comanda:
        
        ```
        sudo netplan apply
        ``` 
        
        La concessió s'haurà de realitzar en aquest moment. D'altra banda, com que aquesta configuració ja queda escrita, serà implementada automàticament cada cop que el sistema client es reinicïi.
        
        Aquest mètode només és vàlid en sistemes Ubuntu (tant en l'edició "Desktop" com "Server" (en aquesta darrera, però, el fitxer a editar es diu **"/etc/netplan/00-installer-config.yaml"**). Podeu saber-ne més si consulteu la seva documentació oficial [aquí](https://netplan.readthedocs.io)
    
    === "Debian Server"
    
        Edita (com administrador) el fitxer **"/etc/systemd/network/enp1s0.network"** per tal que tingui el següent contingut (compte amb les majúscules, han de respectar-se tal com apareixen!)...:
        
        ```ini
        [Match]
        Name=enp1s0
        [Network]
        DHCP=yes
        ```
    
        ...i tot seguit executa la següent comanda:
        
        ```
        sudo networkctl reload
        ```
        
        La concessió s'haurà de realitzar en aquest moment. A més, com que aquesta configuració ja queda escrita, serà implementada automàticament cada cop que el sistema client es reinicïi. 
        
        Els detalls sobre les concessions de xarxa adquirides es poden consultar al directori *"/run/systemd/netif/leases"* però executant la següent comanda...
           
        ```
        ǹetworkctl status enp1s0
        ```
        
        ... podrem obtenir també tota la informació sobre l'estat de la interfície *enp1s0* i, en especial, sobre les seves dades de xarxa actualment assignades.
        
        D'altra banda, si volem demanar en un moment donat aquestes dades de nou (és a dir, renovar la concessió), podrem executar la següent comanda:
     
        ```
        sudo networkctl renew enp1s0
        ``` 
         
        Aquest mètode és vàlid en qualsevol sistema on estigui instal·lat i funcionant el servei Systemd-Networkd. La manera de comprovar que sigui així és observant la sortida de la següent comanda:
        
        ```
        systemctl status systemd-networkd
        ```
    
    === "Fedora Workstation"
    
        Cal executar primer la següent comanda...:
        
        ```
        nmcli connection show
        ```
        
        ... per comprovar si la interfície de xarxa *enp1s0* ja està associada a una connexió; si és així, després d'observar el nom d'aquesta connexió, cal eliminar-la amb la següent comanda:
        
        ```
        nmcli connection del *nomConnexioActual* 
        ```
        
        Tot seguit, cal executar la següent comanda:
        
        ```
        nmcli connection add type ethernet ifname enp1s0 con-name *nomConnexioNova* method auto
        ```
    
        La concessió s'haurà de realitzar immediatament. A més, com que aquesta configuració queda escrita, serà implementada automàticament cada cop que el sistema client es reinicïi. En tot cas, executant la següent comanda...:
        
        ```
        nmcli device show enp1s0
        ```
    
        ... podrem obtenir informació sobre l'estat de la interfície *enp1s0* i, en especial, sobre les seves dades de xarxa actualment assignades.
    
        D'altra banda, si volem demanar en un moment donat aquestes dades de nou (és a dir, renovar la concessió), podrem executar les següents comandes (les quals desactiven la connexió i la tornen a aixecar, respectivament):     
        
        ```
        nmcli connection down *nomConnexioNova* 
        nmcli connection up *nomConnexioNova*
        ``` 
        
        Aquest mètode és vàlid en qualsevol sistema on estigui instal·lat i funcionant el servei NetworkManager. La manera de comprovar que sigui així és observant la sortida de la següent comanda:
        
        ```
        systemctl status NetworkManager
        ```
    
        <hr>
        
        Cal dir que el servei NetworkManager permet ser gestionat també gràficament. Concretament, des de l'apartat "Xarxa" del panell de control de GNOME (l'escriptori per defecte de Fedora Workstation i de moltes altres distribucions), tal com mostra següent captura ...
        
        ![](https://elpuig.xeill.net/Members/q2dg/seguretat-mp11/uf4-1/nmdhcp.png){style="height:80%;width:80%"} 
        
        ... així com també podem observar el resultat obtingut:
        
        ![](https://elpuig.xeill.net/Members/q2dg/seguretat-mp11/uf4-1/nmdades.png){style="height:80%;width:80%"} 


En tot cas, a qualsevol distribució basada en Linux es pot comprovar que la concessió s'ha realitzat correctament observant la sortida de les següents comandes genèriques:

* Per conèixer l'adreça IP assignada: `ip -c address show` 
* Per conèixer la porta d'enllaç per defecte assignada: `ip -c route show` 
* Per conèixer el servidor DNS assignat: `resolvectl dns`


## Correcció de la pràctica

Per confirmar si l'alumne ha realitzat tots els passos d'aquesta pràctica correctament, es suggereixen els següents ítems per comprovar-ho:

- [x] Que la màquina servidora tingui assignada l'adreça IP fixa demanada
- [x] Que el dimoni *kea-dhcp4-server* estigui funcionant
- [x] Que l'arxiu de configuració del servidor Kea estigui formalment ben escrit (és a dir, sigui un JSON vàlid)
- [x] Que la màquina client tingui assignats els valors per la seva adreça IP, porta d'enllaç i servidor DNS de forma coherent amb els indicats a la configuració anterior 
- [x] Que es pugui realitzar un "ping" de forma exitosa entre els dos extrems (client i servidor)

Per automatitzar les comprovacions anteriors, es suggereix l'ús de l'eina de correccions desateses [Teuton](https://github.com/teuton-software/teuton)


## Annex: Aconseguir connexió a Internet pels clients DHCP

La màquina client no té connexió a Internet degut a què només consta d'una interfície de xarxa de tipus "Personal1". Però podem aprofitar que el servidor DHCP té una interfície de tipus "Default" (la qual sí que té connexió a Internet) per "compartir" aquest accés amb la màquina client. Per aconseguir-ho, hem de realitzar els següents  passos:

**1. Cal establir com a porta d'enllaç de la màquina client l'adreça IP de la màquina servidora DHCP**. Això és necessari per a què qualsevol paquet amb un destí de fora de la xarxa interna vagi dirigit a aquesta màquina servidora, que és qui té accés a Internet. Afortunadament, aquest pas ja està fet si s'ha seguit l'enunciat d'aquesta pràctica perquè a l'arxiu *"/etc/kea/kea-dhcp4.conf"* on s'ha establert la configuració d'exemple del servidor Kea, ja s'ha assignat com a valor de l'opció *"routers"* l'adreça IP de la pròpia màquina servidora DHCP.

**2. Cal activar el "IP-Forwarding" en la màquina servidora**. Això és necessari per a què els paquets provinents del client que entrin per la interfície *enp2s0* (la connectada a la xarxa "Personal1") puguin saltar internament a l'altra interfície, la *enp1s0* (la connectada a la xarxa "Default"), en cas que hagin de continuar el seu camí a Internet. Per defecte els sistemes Linux tenen tallada aquesta intercomunicació entre les diverses interfícies de xarxa d'un mateix sistema. Per aconseguir això de forma immediata (tot i que de forma temporal fins que s'apagui el sistema) es pot executar la següent comanda, al servidor:

```
sudo sysctl -w net.ipv4.ip_forward=1
```

<!--   La manera concreta d'aconseguir el mateix efecte de forma permanent depèn de cada distribució, però, en general, és suficient amb editar l'arxiu **"/etc/sysctl.d/99-sysctl.conf"** per tal que hi aparegui la línia següent...:

    ```ini
    net.ipv4.ip_forward=1 
    ```
    
    ...i tot seguit executar la següent comanda:
    
    ```
    sudo sysctl -p
    ```
-->

**3. Cal activar el SNAT a la màquina servidora**. Això és necessari per a què els paquets enviats a Internet semblin que provenen directament de la interfície *enp1s0* (la connectada a la xarxa "Default"). Si no féssim això, aquests paquets serien rebutjats a Internet pel fet de tenir una IP d'origen diferent a la de la interfície que en teoria els està enviant (la *enp1s0*), que és la única que "es veu des de fora". Per aconseguir això hem de configurar el tallafocs del sistema (el qual, en totes les distribucions basades en Linux modernes és [Nftables](https://wiki.nftables.org). Aquest tema dona per [una altra pràctica sencera]() però de manera ràpida, per activar el SNAT immediatament (tot i que de forma temporal fins que s'apagui el sistema) es pot executar les següents comandes, al servidor:

```
sudo nft add table ip nat
sudo nft add chain ip nat postrouting { type nat hook postrouting priority 100 \; }
sudo nft add rule ip nat postrouting ip saddr 192.168.1.0/24 oifname enp1s0 masquerade
```

## Annex: Monitorització gràfica del servidor Kea mitjançant Stork

[Stork](https://stork.isc.org) és un tauler web que permet monitoritzar de forma molt còmoda el funcionament de servidors Kea. Amb Stork podem, per exemple, inspeccionar les seves configuracions, observar com es van realitzant les diverses operacions DHCP, ser alertats també quan ocorre alguna fallada o error, etc. A més, pot integrar-se amb altres eines de monitorització com [Prometheus](https://prometheus.io) i [Grafana](https://grafana.com)

Stork està format per dos components: d'una banda l'"agent Stork" i de l'altra el "servidor Stork"; el primer ha de ser instal·lat juntament amb el servidor Kea perquè és qui interaccionarà directament amb ell per obtenir-ne tota la informació, el segon pot ser instal·lat en una altra màquina "stand-alone" ja que la seva tasca és contactar amb l'agent (o agents si n'hi ha més d'un quan hi ha implementat un clúster de servidors DHCP) i obtenir, a través d'ell, les dades requerides per tal de presentar-les d'una forma integrada i centralitzada.

En tot cas, tota la documentació oficial d'aquest projecte, desenvolupat (a l'igual que Kea) per l'organització ISC, es troba [aquí](https://stork.readthedocs.io)

!!! tip "Fes el desplegament de forma més senzilla"

    És possible instal·lar tant l'agent Stork com el servidor Stork a la mateixa màquina. Això és útil quan només tenim un únic servidor Kea per supervisar i volem evitar haver de tenir un sistema dedicat només per posar en marxa el servidor Stork. De fet, és el que farem als propers apartats: a la mateixa màquina on tenim ja funcionant el servidor Kea no només instal·larem i configurarem l'agent Stork sinó que també instal·larem i configurarem el servidor Stork. 

### Instal·lació del servidor i de l'agent Stork

A continuació instal·larem tant el servidor com l'agent Stork en la mateixa màquina on tenim ja funcionant el servidor Kea. Per fer això, obrirem un terminal en aquesta mateixa màquina i executarem el següent:

!!! example "Instal·lació del servidor Stork "

    === "Ubuntu"
    
        Executa les següents comandes (la primera de les quals serveix per afegir el repositori oficial on es troba disponible Stork):
        
        ```
        curl -1sLf 'https://dl.cloudsmith.io/public/isc/stork/cfg/setup/bash.deb.sh' | sudo bash
        sudo apt install isc-stork-server postgresql
        ```
    
    === "Debian"
    
        Executa les següents comandes (la primera de les quals serveix per afegir el repositori oficial on es troba disponible Stork):
        
        ```
        curl -1sLf 'https://dl.cloudsmith.io/public/isc/stork/cfg/setup/bash.deb.sh' | sudo bash
        sudo apt install isc-stork-server postgresql
        ```
    
    === "Fedora"
    
        Executa les següents comandes (la primera de les quals serveix per afegir el repositori oficial on es troba disponible Stork):
        
        ```
        curl -1sLf 'https://dl.cloudsmith.io/public/isc/stork/cfg/setup/bash.rpm.sh' | sudo bash
        sudo dnf install isc-stork-server postgresql-server postgresql-contrib
        
        ```
Com es pot veure, el servidor Stork necessita per poder funcionar correctament tenir en marxa una instància del SGBD [PostgreSQL](https://www.postgresql.org).

!!! example "Instal·lació de l'agent Stork (només en el cas d'utilitzar el mètode de registre 'agent token', veure més avall el per què)"

    === "Ubuntu"
    
        Executa les següents comandes (la primera de les quals no cal si ja has instal·lat el servidor Stork):
        
        ```
        curl -1sLf 'https://dl.cloudsmith.io/public/isc/stork/cfg/setup/bash.deb.sh' | sudo bash
        sudo apt install isc-stork-agent
        ```
    
    === "Debian"
    
        Executa les següents comandes (la primera de les quals no cal si ja has instal·lat el servidor Stork):
        
        ```
        curl -1sLf 'https://dl.cloudsmith.io/public/isc/stork/cfg/setup/bash.deb.sh' | sudo bash
        sudo apt install isc-stork-agent
        ```
    
    === "Fedora"
    
        Executa les següents comandes (la primera de les quals no cal si ja has instal·lat el servidor Stork):
        
        ```
        curl -1sLf 'https://dl.cloudsmith.io/public/isc/stork/cfg/setup/bash.rpm.sh' | sudo bash
        sudo dnf install isc-stork-agent
        
        ```

### Creació de la base de dades interna del servidor Stork

El primer que hem de fer és crear una base de dades, amb l'estructura de taules adient, que serveixi com a "backend" del servidor Stork. Tot i que això es podria fer amb les eines natives que proporciona el propi PostgreSQL (com seria *psql*), optarem per utilitzar la comanda *stork-tool*, instal·lada juntament amb el servidor Stork, i que està dissenyada precisament per crear i configurar de forma senzilla aquesta base de dades (entre altres tasques, com per exemple gestionar els eventuals certificats TLS utilitzats). Així doncs, el primer que farem serà crear una nova base de dades (anomenada per exemple "stork") i un usuari (anomenat també "stork") amb tots els privilegis sobre aquesta base de dades, i amb contrasenya "stork1234" (també instal·la l'extensió "pgcrypto", necessària pel seu correcte funcionament):

```
sudo -u postgres stork-tool db-create --db-name stork --db-user stork --db-password stork1234
```

### Configuració del servidor Stork i posada en marxa

Cal editar l'arxiu de configuració del servidor Stork, **"/etc/stork/server.env"** , per incloure les credencials indicades al pas anterior. Concretament, cal especificar els valors adients per les següents directives i <u>descomentar-les</u>:

* *STORK_DATABASE_HOST* : L'adreça IP del servidor PostgreSQL (aquí indicarem "localhost")
* *STORK_DATABASE_PORT* : El nombre de port on escolta el servidor PostgreSQL (aquí indicarem 5432, que és el port estàndard per on escolta per defecte aquest servidor)
* *STORK_DATABASE_NAME* : El nom de la base de dades a utilitzar (aquí indicarem "stork", que és el nom que li hem posat a l'apartat anterior)
* *STORK_DATABASE_USER_NAME* : El nom de l'usuari que s'utilitzarà per connectar-s'hi a la base de dades anterior (aquí indicarem "stork", que és el nom que li hem posat a l'apartat anterior)
* *STORK_DATABASE_PASSWORD* : La contrasenya de l'usuari anterior (aquí indicarem "stork1234", que és el valor que li vam assignar a l'apartat anterior)

També és important tenir definides (i descomentades) les següents directives:

* *STORK_REST_HOST* : L'adreça IP per on escoltarà el servidor Stork (i per tant, la que haurem d'indicar per accedir al tauler web); en el cas d'haver seguit aquesta pràctica l'adreça IP que aquí caldrà indicar és la 192.168.1.1. També es pot indicar la IP 0.0.0.0 per establir que es vol que el servidor escolti per qualsevol IP que tingui configurada.
* *STORK_REST_PORT* : El nombre de port on escoltarà el servidor Stork; un valor utilitzat habitualment és el 8081

Un cop guardat l'arxiu "server.env" amb les directives anteriors assignades als valors adients, ja ho tenim tot. Només queda habilitar i arrencar el servidor Stork, així:

```
sudo systemctl --now enable isc-stork-server
```

### Configuració de l'agent Stork i posada en marxa (només en el cas d'utilitzar el mecanisme de registre 'agent token')

Cal editar l'arxiu de configuració del servidor Stork, **"/etc/stork/agent.env"** , per especificar els valors adients per les següents directives i <u>descomentar-les</u>:

* *STORK_AGENT_HOST* : L'adreça IP (o nom DNS) per on l'agent Stork rebrà les connexions provinents del servidor Stork per obtenir-ne les dades; en el cas d'haver seguit aquesta pràctica l'adreça IP que aquí caldrà indicar és la 192.168.1.1. 
* *STORK_AGENT_PORT* : El nombre de port per on l'agent Stork rebrà les connexions provinents del servidor Stork; aquí indicarem el valor 8080
* *STORK_AGENT_SERVER_URL* : Aquesta directiva només es té en compte si es realitza el registre de l'agent al servidor mitjançant el mètode del "agent token" en lloc del mètode del "server token" (seguir llegint per saber-ne la diferència). En aquesta pràctica es farà servir el primer mètode, així que caldrà definir-la. El seu valor és la URL del servidor Stork on l'agent enviarà les dades i comandes REST necessàries per realitzar el seu registre. En el nostre cas, serà <i><u>http://192.168.1.1:8081</i></u>

Un cop guardat l'arxiu "server.env" amb les directives anteriors assignades als valors adients, ja ho tenim tot. Només queda, <u> després d'haver-nos assegurat que el servidor DHCP Kea està en marxa</u>, habilitar i arrencar el servidor Stork, així:

```
sudo systemctl --now enable isc-stork-agent
```

### Accés al panell web i registre de l'agent

Per accedir al panell web només cal obrir un navegador i escriure a la barra de direccions la URL <i><u>http://192.168.1.1:8081</i></u> . S'arribarà a la següent pàgina...:

![](servidor_dhcp_kea_assets/stork1.png){style="height:100%;width:100%"}

...on caldrà indicar l'usuari *"admin"* amb contrasenya *"admin"* per poder entrar al següent tauler:

![](servidor_dhcp_kea_assets/stork2.png){style="height:100%;width:100%"}

!!! warning "Bona pràctica de seguretat"

    El primer que caldria fer, un cop a l'interior d'aquest panell web, hauria de ser canviar la contrasenya de l'usuari *"admin"*. Per fer això, cal anar al menú "Configuration → Users" i, un cop allà, pitjar sobre el nom de l'usuari en qüestió per tal d'accedir al formulari d'edició del seu perfil, on, entre altres dades, es pot canviar la seva contrasenya.

Si vam instal·lar el paquet de l'agent Stork i vam seguir els passos de la seva configuració indicats a l'apartat anterior, el primer que hem de fer ara és aprovar el registre d'aquest agent Stork (associat, recordem-ho al servidor Kea que tenim en marxa), per tal que comencin a aparèixer les seves dades al tauler web. Per fer això, cal anar al menú "Services → machines" i pitjar el botó "Unauthorized" que apareix a la banda dreta de la pàgina; hi hauria d'aparèixer llavors llistat l'agent que acabem de configurar al pas anterior, pendent de registre. Des d'aquesta nova pàgina el podrem "registrar" si cliquem sobre l'opció "Authorize" (tal com mostra la següent captura);

![](servidor_dhcp_kea_assets/stork3.png){style="height:100%;width:100%"}

D'aquesta manera, la màquina en qüestió passarà a visualitzar-se a partir d'ara sota la pestanya de les màquines "Authorized":

![](servidor_dhcp_kea_assets/stork4.png){style="height:100%;width:100%"}

D'altra banda, si pitjem sobre la IP de l'agent mostrat, es pot accedir a una pantalla on es pot observar diferent informació sobre el sistema en sí i el seu estat de funcionament.

![](servidor_dhcp_kea_assets/stork5.png){style="height:100%;width:100%"}

A partir d'aquí, si anem al menú "Services → Kea Apps" podrem conèixer més informació sobre la configuració del servidor DHCP i, més en concret, si anem a les diverses opcions del menú "DHCP", podrem conèixer els prèstecs realitzats, les reserves preestablertes, etc. A continuació es mostren un parell de captures (extretes de la pàgina oficial del projecte) on s'hi mostren aquestes àmplies possibilitats

![](servidor_dhcp_kea_assets/stork6.png){style="height:100%;width:100%"}

![](servidor_dhcp_kea_assets/stork7.png){style="height:100%;width:100%"}

!!! info "Sobre el mètode de registre 'server token'"

    En aquesta pràctica s'ha indicat com fer el registre fent servir el mètode "agent token", mitjançant el qual, un cop preparada adientment la configuració de l'agent, aquest aguarda a que la petició de registre sigui validada manualment des de la interfície web del servidor Stork. L'altre mètode, anomenat "server token", consisteix en els següents passos:
    
    * No instal·lar cap agent com s'ha explicat a l'apartat anterior
    
    * Entrar dins del panell web del servidor Stork i anar a la pàgina "Services → machines" . Allà, pitjar sobre el botó "How to Install Agent on New Machine“. Una nova finestra haurà d'aparèixer mostrant unes determinades comandes de terminal que caldrà executar tal qual a la màquina on es troba el servidor DHCP Kea funcionant: aquestes comandes són les que descarregaran l'agent del servidor Stork i l'instal·laran en aquest moment a la màquina on s'executin. Durant aquesta instal·lació es demanarà de forma interactiva un "token" (és a dir, una cadena alfanumèrica) que és justament el mostrat a la finestra del panell web.
    
    Fent servir aquest mètode de registre, l'agent instal·lat ja apareixerà directament "autoritzat" al panell web del servidor Stork
    

