# Ús bàsic del sistema operatiu Windows

L'objectiu d'aquesta pràctica és familiaritzar-se amb les possibilitats que ofereix l'eina de facturació FactuSol


## Cicles, Mòduls i UFs on es pot realitzar aquesta pràctica

* **GA   MP02** UF1:Circuit administratiu de la compravenda
* **GA   MP02** UF2:Gestió d'estocs
* **GA   MP02** UF3:Declaracions fiscals derivades de la compravenda
* **AiF  MP10** UF1:Planificació de l'aprovisionament
* **AiF  MP10** UF2:Selecció i control de proveïdors
* **AiF  MP10** UF3:Operativa i control en la cadena logística


## Infraestructura prèvia necessària per implementar aquesta pràctica

!!! warning "Vigila l'ús de vCPUs i de RAM!"

    És **molt important** que la quantitat de vCPUs i de memòria RAM assignades a les màquines utilitzades en aquesta pràctica sigui el més adjustada possible!!
    
    La quantitat global d'hores que un centre educatiu té disponible per utilitzar IsardVDI (les anomenades "DHN", de "Desktop Hours Number") ve donada per la següent [fòrmula](https://isard.gitlab.io/isardvdi-docs-pilot-fp/usage/usage.ca/#__tabbed_1_1). Això implica que quanta més vCPUs i -sobre tot- RAM s'assignin a les màquines creades dins d'un centre concret, menys hores d'Isard aquest centre podrà consumir en total.

!!! info "Idees per no consumir gaire RAM"

    * Utilitzar distribucions basades en Linux de tipus "Server" (és a dir, sense entorn gràfic). Exemples de distribucions d'aquest tipus són, per exemple, [Debian Server](https://www.debian.org), [Ubuntu Server](https://ubuntu.com/download/server), [Fedora Server](https://fedoraproject.org/server), [openSUSE Server](https://get.opensuse.org/server), [Rocky Linux Server](https://rockylinux.org) o [Alma Linux Server](https://almalinux.org), entre moltes altres. 
    
    * Utilitzar distribucions que, tot i tenir un entorn gràfic, aquest sigui més minimalista que els entorns estàndard (com GNOME o KDE) i, per tant, més lleuger de recursos. Exemples de distribucions d'aquest tipus són: [Slitaz](https://www.slitaz.org), [Puppy](https://puppylinux-woof-ce.github.io), [Slax](https://www.slax.org), [Bodhi](https://www.bodhilinux.com), [Lubuntu](https://lubuntu.me), [Fedora LXQT](https://fedoraproject.org/spins/lxqt) o fins i tot [Fedora Sway](https://fedoraproject.org/spins/sway), entre moltes altres
       
    En tot cas, si a l'hora de voler crear un escriptori s'optés per triar alguna de les distribucions anteriors, caldria comprovar primer que IsardVDI ofereix la plantilla-base corresponent; si no és així, llavors caldria realitzar manualment la instal·lació de l'escriptori desitjat a partir del medi ISO adient (tal com s'explica [aquí](https://isard.gitlab.io/isardvdi-docs/advanced/media)). 
    
    Si aquest sistema instal·lat es volgués utilitzar, a més, per fer un desplegament, llavors caldria primer crear a partir de l'escriptori anterior una plantilla pròpia (tal com s'explica [aquí](https://isard.gitlab.io/isardvdi-docs/advanced/templates/#create)) i tot seguit fer el desplegament a partir d'ella (tal com s'explica [aquí](https://isard.gitlab.io/isardvdi-docs/advanced/deployments)).

!!! tip "Utilitza el visor SPICE per millorar la teva experiència d'usuari"

    A l'hora de triar un visor per a les teves màquines, et recomanem que utilitzis el visor SPICE ja que ofereix, a més de molt poca latència, una major funcionalitat i flexibilitat que els altres visors disponibles. Per exemple, mitjançant aquest visor podràs gaudir de l'àudio integrat i seràs capaç de copiar-pegar entre la teva màquina real local i les màquines Isard (si no són de tipus "Server"), així com també connectar-hi dispositius USB en la primera i que siguin visibles en les segones, entre altres possibilitats. Per saber més informació sobre les característiques dels diferents visors disponibles, pots consultar [el manual](https://isard.gitlab.io/isardvdi-docs/user/viewers/viewers)


Per realitzar aquesta pràctica només es necessitarà disposar d'una màquina virtual, la qual pot consistir en un desplegament ofert pel professor a tot el grup-clase o bé un escriptori generat per cada alumne individualment, tant se val. A continuació es detallen els passos a fer en qualsevol d'aquestes dues opcions.

!!! example "Creació de la màquina de treball"

    === "En forma de desplegament"
    
        1. Triar l'opció "Desplegaments del menú superior i tot seguit pitjar en el botó "Desplegament nou"
        2. Triar, al formulari web que apareix, les característiques que desitjem que tingui la màquina. En destacarem les que són rellevants per aquesta pràctica:
            + **Visible**: SÍ (caldrà indicar a més, en l'apartat corresponent, el grup i/o alumnes concrets als quals se'ls vol mostrar la màquina)
            + **vCPUs** i **RAM**: És important tenir en compte el que s'indica al primer quadre d'aquesta pàgina  
            + **Plantilla base**: Qualsevol de tipus Windows. És important tenir en compte, però, el que s'indica al segon quadre d'aquesta pàgina  
            + **Visors**: És important tenir en compte el que s'indica al tercer quadre d'aquesta pàgina  
        3. Pitjar el botó "Crear". 
        
        En tot cas, teniu més informació sobre com crear desplegaments i les possibilitats que aquests ofereixen [aquí](https://isard.gitlab.io/isardvdi-docs/advanced/deployments)
    
    === "En forma d'escriptori"
    
        1. Pitjar en el botó "Escriptori nou"
        2. Triar, al formulari web que apareix, les característiques que desitjem que tingui la màquina. En destacarem les que són rellevants per aquesta pràctica:
            + **vCPUs** i **RAM**: És important tenir en compte el que s'indica al primer quadre d'aquesta pàgina  
            + **Plantilla base**: Qualsevol de tipus Windows. És important tenir en compte, però, el que s'indica al segon quadre d'aquesta pàgina 
            + **Visors**: És important tenir en compte el que s'indica al tercer quadre d'aquesta pàgina    
        3. Pitjar el botó "Crear".     
        
        En tot cas, teniu més informació sobre com crear escriptoris i les possibilitats que aquests ofereixen [aquí](https://isard.gitlab.io/isardvdi-docs/user/create_desktop)


## Enunciat de la pràctica

* Instal·lació FactuSol
* Creació d'empresa. Còpia de seguretat. Esborrar empresa, restaurar, obrir
* Banc, Moneda, Divises, Agent comercial
* Formes de pagament/cobrament i contrapartides
* Clients
* Proveïdors
* Magatzem, articles
* Operacions: compres i valoració estoc
* Operacions: Pressupostos i comandes
* Operacions: Comandes, albarans i factures
* Operacions: Comandes, factures i cobraments



