# Introducció

En aquest apartat es detallaran que són les plantilles base, com reconèixer-les amb les nomenclatures, cada quant s'actualitzen, i els programaris dins de cadascuna de les plantilles base d'Isardvdi que hi ha disponible (si tenen).

Totes les plantilles tenen com a base aquestes credencials:

Usuari: isard
Contrasenya: pirineus


## Què és una plantilla base

Una plantilla base és un model o imatge preconfigurada d'un escriptori virtual que s'utilitza com a punt de partida per a crear i desplegar escriptoris virtuals addicionals.

La plantilla base d'un escriptori virtual inclou una configuració específica de sistema operatiu, aplicacions, configuracions de xarxa i  altres elements que són necessaris per al funcionament de l'escriptori virtual. Quan es crea un nou escriptori virtual, s'utilitza la plantilla base com a punt de partida per a crear una còpia del sistema operatiu i les aplicacions preinstal·lades.

En utilitzar una plantilla base per a crear i desplegar escriptoris virtuals addicionals, es pot reduir el temps i l'esforç necessaris per a configurar i personalitzar cada escriptori virtual de manera individual. A més, la plantilla base pot ajudar a mantenir la consistència i la coherència entre els escriptoris virtuals, ja que tots els escriptoris virtuals creats a partir de la mateixa plantilla base tindran la mateixa configuració i conjunt d'aplicacions preinstal·lades.


## Nomenclatura de les plantilles base

### Nom de les plantilles

Les plantilles portaran el nom del sistema operatiu, el d’Isard i un número de versió:

**SistemaOperatiu_Isardvdi_vX:**
- Sistema operatiu: El nom oficial del sistema operatiu.

- Isardvdi: Perquè se sàpiga que és una plantilla oficial d'Isard
- vX: Versió 
    - vX.X:
        -	**La primera X**: actualitzacions del sistema
        -	**La segona X**: són les actualitzacions del software

**Exemple de versions:** 

Si tenim una versió **v1.0**

- Si s'actualitza el sistema es canvia la xifra de la versió tal que així: **v2.0**
- Si s'actualitza el software: **v1.1**

### Descripció de les plantilles

En la descripció portarà:

- La data en la qual es va crear/va modificar. Ex: 23/03/2023
- Usuari/Contrasenya per a poder entrar (encara que haurien de tenir totes les plantilles el autologin però per si el sistema demana alguna contrasenya,etc)

**Exemple:**
Verificada per IsardVDI 21032023. Usuari: isard, Contrasenya: pirineus


## Seguiment de versions

Cada vegada que s'actualitzi una plantilla, es desactivarà l'anterior i així cap usuari continuarà creant un escriptori a partir d'una plantilla desfasada.

Això comporta actualitzacions de programes i sistema.


## Programari dins de les plantilles base

(No totes les plantilles tenen programari instal·lat)

- **Windows 10 T3_Isardvdi_vX:** Windows amb els Vmware Tools passades i assegurar-se que estigui el RDP activat. Treure les instal·lacions automàtiques. Amb firefox actualitzat i treure les instal·lacions automàtiques.
    - Adobe Acrobat Reader: Programari per a visualitzar, crear i modificar arxius amb el format PDF.
    - LibreOffice: Paquet d'oficina lliure.
    - Gimp: Programari d'edició d'imatges.
    - LibreCAD: Programari lliure per a disseny 2D.
    - Inkscape: Editor de gràfics vectorials lliure. Es poden editar diagrames, línies, gràfics, logotips i il·lustracions.
    - Geany: Editor de text.
    - Chrome, Firefox: Navegadors d'Internet.

- **Windows 10 T2_Isardvdi_vX:** Windows amb programari lliure base i modificació per a treure el tallafoc
    - Adobe Acrobat Reader: Programari per a visualitzar, crear i modificar arxius amb el format PDF.
    - LibreOffice: Paquet d'oficina lliure.
    - Gimp: Programari d'edició d'imatges.
    - LibreCAD: Programari lliure per a disseny 2D.
    - Inkscape: Editor de gràfics vectorials lliure. Es poden editar diagrames, línies, gràfics, logotips i il·lustracions.
    - Geany: Editor de text.
    - Chrome, Firefox: Navegadors d'Internet.

- **Windows 11 T3_Isardvdi_vX:** Windows amb els Vmware Tools passades i assegurar-se que estigui el RDP activat. Treure les instal·lacions automàtiques. Amb firefox actualitzat i treure les instal·lacions automàtiques.
    - Adobe Acrobat Reader: Programari per a visualitzar, crear i modificar arxius amb el format PDF.
    - LibreOffice: Paquet d'oficina lliure.
    - Gimp: Programari d'edició d'imatges.
    - LibreCAD: Programari lliure per a disseny 2D.
    - Inkscape: Editor de gràfics vectorials lliure. Es poden editar diagrames, línies, gràfics, logotips i il·lustracions.
    - Geany: Editor de text.
    - Chrome, Firefox: Navegadors d'Internet.

- **Windows 11 T2_Isardvdi_vX:** Windows amb programari lliure base i modificació per a treure el tallafoc
    - Adobe Acrobat Reader: Programari per a visualitzar, crear i modificar arxius amb el format PDF.
    - LibreOffice: Paquet d'oficina lliure.
    - Gimp: Programari d'edició d'imatges.
    - LibreCAD: Programari lliure per a disseny 2D.
    - Inkscape: Editor de gràfics vectorials lliure. Es poden editar diagrames, línies, gràfics, logotips i il·lustracions.
    - Geany: Editor de text.
    - Chrome, Firefox: Navegadors d'Internet.

- **Windows Server 2019 T3_Isardvdi_vX**

    - Login: Administrador/Aneto_3404

- **Windows Server 2022 T3_Isardvdi_vX**

    - Login: Administrador/Aneto_3404

- **Linkat 22.04_Isardvdi_vX**

- **Ubuntu 22.04 Desktop_Isardvdi_vX**
    - LibreOffice: Paquet d'oficina lliure.
    - Chromium, Firefox: Navegadors d'Internet.
    - Remmina: Programari que permet la connexió de manera remota als escriptoris virtuals 
    - VLC media player: És un reproductor i framework multimèdia, lliure i de codi

- **Ubuntu 22.04 Server_Isardvdi_vX**
    - LibreOffice: Paquet d'oficina lliure.
    - Chromium, Firefox: Navegadors d'Internet.
    - VLC media player: És un reproductor i framework multimèdia, lliure i de codi

- **Fedora 38 Workstation_Isardvdi_vX**
    - LibreOffice: Paquet d'oficina lliure.
    - Firefox: Navegadors d'Internet.

- **Debian 12 Desktop_Isardvdi_vX**
    - LibreOffice: Paquet d'oficina lliure.
    - Chromium, Firefox: Navegadors d'Internet.
    - VLC media player: És un reproductor i framework multimèdia, lliure i de codi

- **Debian 12 Server_Isardvdi_vX**

- **Z install Sistema Operatiu_Isardvdi**