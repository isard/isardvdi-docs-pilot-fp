# SAML

Para poder acceder por ejemplo desde una cuenta del departamento de educación mediante un código de registro proporcionado por un Manager.

Desde la página principal se selecciona la categoría a la que va a pertenecer el usuario y se pulsa el botón ![](./saml_access.ca.images/saml_access1.png)

![](./saml_access.es.images/saml_access1.png)

Se redirecciona al portal de iniciar sesión y se rellena con el correo.

![](./saml_access.es.images/saml_access2.png)

Se escribe la contraseña de la cuenta y se pulsa el botón de "Iniciar Sesión"

![](./saml_access.es.images/saml_access3.png)

Si se quiere mantener la sesión iniciada se pulsa el botón "Si"

![](./saml_access.es.images/saml_access4.png)

## Inscripción por código de registro

Si es la primera vez que se accede como usuario aparecerá el formulario para introducir un código de registro. 

El código de registro se crea por parte del manager de la organización y se explica cómo se genera más adelante en la sección de "Manager" de la documentación. Cada usuario ha de pertenecer a un grupo y tener un rol, ya sea advanced (profesor), user (alumno), etc. Por cada grupo de usuarios se pueden generar códigos de autoregistro para diferentes roles. 

Se introduce el código proporcionado por un Manager y se pulsa el botón ![](./saml_access.es.images/saml_access5.png)

![](./saml_access.es.images/saml_access6.png)

Y se accede a la interfaz básica autenticados con la cuenta de SAML.

Una vez autenticados, ya no hace falta repetir el proceso de registro. Cuando se quiera volver a acceder mediante SAML, sólo se tiene que pulsar en el botón ![](./saml_access.ca.images/saml_access1.png) y utilizar las credenciales de la cuenta.