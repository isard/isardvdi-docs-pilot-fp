# IDI-RALC

Per a poder accedir per exemple des d'un compte del departament d'educació mitjançant un codi de registre proporcionat per un Manager.

Des de la pàgina principal se selecciona la categoria a la qual pertanyerà l'usuari i es prem el botó ![](./saml_access.ca.images/saml_access1.png)

![](./saml_access.ca.images/saml_access2.png)

Es redirigeix al portal d'iniciar sessió i s'emplena amb el correu.

![](./saml_access.ca.images/saml_access3.png)

S'escriu la contrasenya del compte i es prem el botó de "Iniciar Sessió"

![](./saml_access.ca.images/saml_access4.png)

Si es vol mantenir la sessió iniciada es prem el botó "Si"

![](./saml_access.ca.images/saml_access5.png)

## Inscripció per codi de registre

Si és la primera vegada que s'accedeix com a usuari apareixerà el formulari per a introduir un codi de registre.

El codi de registre es crea per part del manager de l'organització i s'explica com es genera més endavant en la secció de "Manager" de la documentació. Cada usuari ha de pertànyer a un grup i tenir un rol, ja sigui advanced (professor), user (alumne), etc. Per cada grup d'usuaris es poden generar codis d'autoregistre per a diferents rols. 

S'introdueix el codi proporcionat per un Manager i es prem el botó ![](./saml_access.ca.images/saml_access6.png)

![](./saml_access.ca.images/saml_access7.png)

I s'accedeix a la interfície bàsica autenticats amb el compte de SAML.

Una vegada autenticats, ja no fa falta repetir el procés de registre. Quan es vulgui tornar a accedir mitjançant SAML, només s'ha de prémer en el botó ![](./saml_access.ca.images/saml_access1.png) i utilitzar les credencials del compte.