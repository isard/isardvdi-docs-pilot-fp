# Company Portal 

Aquest manual vol posar a disposició de tots els usuaris que utilitzin equips Windows 10, dintre del Pla d’educació digital de Catalunya, el nou Portal d’Aplicacions corporatiu – Company Portal

## Descripció del servei

El Company Portal és el nou servei corporatiu des d’on es pot accedir al catàleg d’aplicacions homologades per a realitzar la instal·lació de manera autònoma als ordinadors Windows 10, tant d’alumnes com de docents, dins del Pla d’educació digital de Catalunya.

Les aplicacions que s’hi troben hauran estat validades per l’equip tècnic que dona suport a l’estació de treball i han passat els criteris de seguretat i idoneïtat marcats pel Departament d’Educació, assegurant el manteniment posterior en referència a versions o pegats de seguretat.

L’organització d’aquest portal està basat en un grup de categories preestablert, per tal de navegar més còmodament per l’entorn en la cerca de les aplicacions. Contínuament s’anirà actualitzant el catàleg d’aplicacions disponibles, afegint-ne de noves que la comunitat educativa consideri necessàries, i traient-ne aquelles que queden obsoletes.

## Com utilitzar-ho

### Accés a l'entorn

Per poder accedir-hi, es prem al menú inici i es selecciona l'aplicació de Company portal.

![](./company_portal.images/companyportal1.png){width="50%"}

### Navegació per les aplicacions

Un cop executada l’aplicació, dins l’entorn gràfic, hi han diferents opcions per revisar si l’aplicació que es requereix està disponible. En l’entorn gràfic es disposa de diferents opcions per tal de cercar les aplicacions. Per defecte, el sistema proposa les darreres afegides.

**Apps:** Apareixen totes les aplicacions disponibles per descarregar, a la part dreta es pot filtrar per categories.

![](./company_portal.images/companyportal2.png){width="50%"}

**App categories:** Apareixen només les aplicacions de la categoria seleccionada.

![](./company_portal.images/companyportal3.png){width="50%"}

### Instal·lació

Un cop s'ha trobat l’aplicació que es necessita només s'haurà de seleccionar-la i es prem sobre “Instal·lar”. Si no es troba l’aplicació desitjada, l’equip directiu o el coordinador digital del vostre centre podrà sol·licitar l’homologació i posada a disposició de l’aplicació no disponible realitzant la petició al portal PAUTIC (https://pauticgencat.onbmc.com) i justificant la necessitat, omplint el formulari corresponent (punt 4 d’aquest document).

![](./company_portal.images/companyportal4.png){width="50%"}

## Instal·lació virt-viewer 11

En aquest apartat del manual d'usuari d'Isardvdi, es troben instruccions detallades per a la instal·lació del virt-viewer 11. És important destacar que per a fer servir el **visor Spice**, és necessari comptar amb el programari 'virt-viewer'. Aquestes instruccions han estat elaborades per a brindar un pas a pas precís i clar en la instal·lació i configuració del visor Spice, permetent-te aprofitar al màxim les capacitats de la nostra plataforma Isardvdi.

### Accés a l'entorn

Per poder accedir-hi, es prem al menú inici i se selecciona l'aplicació de **Company portal**.

![](./company_portal.images/companyportal1.png){width="50%"}

### Instal·lació

S'ha de cercar l'aplicació **"virt-viewer"** a la barra de cerca i seleccionar **"Virt-viewer + usb"** per a la instal·lació, juntament amb el paquet que inclou l'instal·lador d'UsbDk per permetre l'ús dels dispositius USB.

![](./virt-viewer/virt1.png){width="45%"}
![](./virt-viewer/virt2.png){width="45%"}

Una vegada s'ha realitzat la instal·lació, per poder instal·lar el paquet UsbDK, se segueixen aquests passos:

![](./virt-viewer/virt5.png){width="45%"}
![](./virt-viewer/virt6.png){width="45%"}
![](./virt-viewer/virt7.png){width="45%"}
![](./virt-viewer/virt8.png){width="45%"}

!!! Warning
    Sol·licitarà credencials d'administrador per poder dur a terme la instal·lació del paquet **UsbDk**. Caldrà posar-se en contacte amb el vostre coordinador.

Una vegada s'ha instal·lat, podem fer les proves amb el **visor Spice**. S'engega l'escriptori i se selecciona "Spice":

![](./virt-viewer/virt12.png){width="45%"}

### Dispositius USB

**Si el que volem és, utilitzar dispositius USB:** 

1. S'introdueix el dispositiu al nostre ordinador físic. **Important** deixar temps perquè l'ordinador trobi el dispositiu.
2. Se selecciona la segona icona de dalt a l'esquerra

    ![](./virt-viewer/virt9.png){width="45%"}

3. Se selecciona el dispositiu USB que es vol utilitzar i ja es podrà visualitzar en el nostre escriptori virtual.  

!!! Important
    És possible que aquesta acció tardi uns segons.

![](./virt-viewer/virt10.png){width="45%"}
![](./virt-viewer/virt11.png){width="45%"}