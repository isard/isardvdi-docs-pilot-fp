# Introducció

**Plataforma departamental d'Educació "IsardVDI - Escriptoris Virtuals"**

Va néixer a l'FP, concretament a L'Escola del Treball l'any 2014. A finals de l'any 2020 es va iniciar un pilot a 5 centres d'FP. A inicis de l'any 2024 va finalitzar el pilot satisfactòriament, amb un total de 99 centres d'FP. Els principals avantatges d'aquesta plataforma són:

- **Per a l’alumnat**
    - Pràctiques poden quedar associades a un escriptori preparat a mida.
    - Alumne pot utilitzar des de casa amb les mateixes eines.

- **Pel professorat**
    - Major autonomia, gràcies a poder crear les seves pròpies plantilles.
    - Major agilitat i control, gràcies als desplegaments.

- **Avantatges per la gestió TAC al centre**
    - Treure feina al coordinador informàtic.
    - Major mobilitat, gràcies al fet que no estigui associat el lloc de treball a un lloc físic.
    - Major compatibilitat a nivell de programari, gràcies al fet que no ve determinat pel maquinari de l'equip físic.

A destacar també que tots els anys que hem estat de pilotatge han permès afegir funcionalitats que han estat demandades pel professorat. Així com l'alt grau de satisfacció que han traslladat els centres participants.

Per a conèixer com s'utilitza el servei a la plataforma i les possibilitats que ofereix, teniu a la vostra disposició els següents vídeos:

- [Vídeo de presentació “Què és IsardVDI i què ofereix?”](https://youtu.be/hm1TdKbWDCQ)
- [Vídeo de presentació “Tipus de visors”](https://youtu.be/15aaRbkFUuk)
- [Vídeo de presentació “Plantilles en acció”](https://youtu.be/6WktK3R7dTQ)
- [Vídeo de presentació “Més enllà d'un escriptori virtual”](https://youtu.be/5QQBKTEl4zM)
- [Videotutorial “Com crear usuaris i grups”](https://youtu.be/cFy1Fr1rDyE)
- [Videotutorial “Com crear escriptoris i plantilles”](https://youtu.be/-nKtud9A_xo)
- [Videotutorial “Com duplicar plantilles”](https://youtu.be/FisGe3Unkew)
- [Videotutorial “Com crear un desplegament”](https://youtu.be/aaeNXC-Qyv0)

A la plataforma també disposeu d’un apartat d’Ajuda on trobareu tota la informació necessària per utilitzar aquesta eina.
