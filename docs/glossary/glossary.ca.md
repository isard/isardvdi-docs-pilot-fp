# Glossari

Aquest glossari és una valuosa referència per a entendre els termes específics utilitzats en la plataforma d'IsardVDI. Cada paraula i concepte s'explica de manera concisa i clara, la qual cosa t'ajudarà a navegar i comprendre millor aquesta plataforma de virtualització d'escriptoris. Aquest recurs és útil per a principiants i usuaris experimentats que busquen aclarir qualsevol dubte en la seva experiència amb IsardVDI.


**Boot:** Procés d'inici del sistema, que determina des d'on s'inicia el sistema operatiu o programari principal.

**Bulk create:** Aquesta funció permet la creació simultània de múltiples usuaris a la plataforma de manera massiva.

**Bulk edit:** Aquesta funció permet l'edició simultània de múltiples escriptoris virtuals de manera massiva.

**Bulk add desktop:** Aquesta funció permet la creació simultània de múltiples escriptoris virtuals per a assignar-los a usuaris, grups o categories específiques de manera massiva.

**Change owner:** Funcionalitat que canvia la propietat d'escriptoris, plantilles i mitjans, com l'emmagatzematge del domini, de propietari (incloent-hi grup i categoria).

**CPU:** És l'encarregat de processar i executar totes les tasques i càlculs necessaris perquè l'ordinador funcioni, com obrir programes, realitzar càlculs matemàtics o mostrar imatges en la pantalla. Essencialment, és el component principal que pren les decisions i executa les instruccions per a fer funcionar el teu ordinador.

**Desplegament:** És una creació massiva d'escriptoris virtuals per a diversos grups o usuaris, amb la capacitat de supervisar i gestionar tots aquests escriptoris de manera centralitzada.

**Disk bus:** És el camí pel qual les dades viatgen des de i cap al disc dur, permetent a l'ordinador accedir a la informació emmagatzemada i fer tasques. Un disk bus eficient és important per a una ràpida transferència de dades i un bon rendiment general del sistema.

**Emmagatzematge:** Apartat on es pot obtenir més d'informació sobre els discs dels escriptoris que s'han creat, així com el consum d'aquests i de plantilles.

**Escriptori virtual:** Són imatges preconfigurades de sistemes operatius i aplicacions en els quals l'entorn d'escriptori està separat del dispositiu físic utilitzat per a accedir a ell. Els usuaris poden accedir als seus escriptoris virtuals de manera remota i amb qualsevol dispositiu.

**GPU:** És un processador especialitzat en el maneig de gràfics, dissenyat per a alleujar la càrrega de treball de la CPU en aplicacions com a videojocs i programari 3D.

**Hardware:** Són tots els components físics que formen part de la infraestructura necessària per al funcionament dels escriptoris virtuals.

**Hipervisor:** És el que s'encarrega d'assignar i gestionar els recursos entre totes les màquines virtuals. Els dona a cada màquina virtual el seu propi espai i recursos, permetent que funcionin de manera independent les unes de les altres, com si fossin diferents ordinadors.

**ISO:** Imatge binària d'un disc CD/DVD, que representa una còpia exacta del contingut del disc en un arxiu únic, utilitzat per a distribuir i emmagatzemar dades, programari i sistemes operatius.

**Límits:** Els límits defineixen la quantitat de recursos que es poden utilitzar globalment en una categoria o un grup.

**Mitjans:** Apartat on es poden pujar isos (imatges de discos).

**Plantilla:** És una còpia d'un escriptori virtual que conserva l'estat del sistema, la qual cosa facilita la creació de múltiples escriptoris basats en aquesta configuració inicial.

**Quotes:** Restriccions que s'apliquen a un usuari en crear escriptoris, plantilles, carregar mitjans i altres accions dins del sistema. Aquestes restriccions defineixen els límits o les quantitats permeses per a unes certes operacions, assegurant un ús equitatiu dels recursos i la capacitat de gestió del sistema.

**RAM:** És un espai temporal on l'ordinador guarda informació i programes que està utilitzant en aquest moment per a poder accedir ràpidament a ells. Com més RAM tingui un ordinador, més coses pot fer servir al mateix temps sense tornar-se lenta, com obrir diversos programes o treballar amb arxius grans.

**Server:** Mode d'encesa permanent per a un escriptori virtual. Quan s'activa aquest mode, l'escriptori virtual romandrà encès de manera contínua, sense apagar-se automàticament.

**Status:** És l'estat en el qual es troba l'escriptori virtual en un moment específic.

- **Creating Domain:** Estat en el qual s'està creant l'escriptori virtual.
- **Starting:** Estat en el qual s'està arrancant l'escriptori virtual.
- **Started:** Estat en el qual ja està arrancat l'escriptori virtual.
- **Stopping:** Estat en el qual s'està aturant l'escriptori virtual.
- **Stopped:** Estat en el qual ja està aturat l'escriptori virtual.
- **Failed:** Estat en el qual es queden els escriptoris quan alguna cosa falla en arrancar.

**Videowall:** És una funció que permet veure totes les pantalles de tots els escriptoris d'un desplegament particular simultàniament i en temps real. També permet interactuar amb qualsevol dels escriptoris mostrats i utilitzar-los com a propis.

**Visor:** Un visor és una eina que s'utilitza per a visualitzar i accedir a un escriptori virtual o remot, permetent-te interactuar amb un entorn informàtic des d'una ubicació distant o a través d'una plataforma de virtualització.

**Visor directe:** És un enllaç que permet la visualització d'un escriptori sense necessitat d'accedir físicament a l'escriptori en si.

**Xarxes:**

- **Xarxa personal:** Aquesta xarxa es configura a múltiples escriptoris a la vegada per establir una comunicació entre màquines del mateix usuari.
- **Xarxa privada:** Aquesta xarxa es configura a múltiples escriptoris a la vegada per establir una comunicació entre màquines de múltiples usuaris. 
- **Default:** Proporciona una adreça que canvia automàticament (anomenada DHCP) amb un accés a Internet a través d'un dispositiu de connexió compartida (anomenat encaminador NAT). Aquesta direcció també inclou informació de DNS. A més, aquest escriptori virtual està separat d'uns altres en el sistema, és com si estigués en el seu propi espai sense connexió directa amb els altres.
- **Wireguard VPN:** Aquesta és una xarxa del sistema que assigna una adreça IP a l'escriptori virtual i és necessària per a connectar-se a l'escriptori des de casa i permetre connexions de visualització de tipus **RDP**.

**XML:** Fitxer de configuració de la màquina virtual.

