# Vídeos

En aquesta part del manual, hem dissenyat una experiència d'aprenentatge visual per a ajudar-te a familiaritzar-te i aprofitar al màxim la nostra plataforma.

Aquests breus, però informatius vídeos et guiaran, pas a pas, a través de cada aspecte essencial de la nostra plataforma. Independentment del teu nivell d'experiència, els nostres tutorials estan dissenyats per a ser clars i fàcils de seguir.

## Videotutorials

### Com crear usuaris i grups

En aquest videotutorial, et guiarem a través del procés de [creació d'usuaris](https://isard.gitlab.io/isardvdi-docs/manager/manage_user.ca/#creacio-dusuaris) i [grups](https://isard.gitlab.io/isardvdi-docs/manager/manage_user.ca/#creacio-de-grups) en IsardVDI, una tasca fonamental per a administrar i organitzar el teu entorn virtual de manera eficient.

Aprendràs a crear usuaris individuals i grups específics segons les teves necessitats.

<iframe width="560" height="315" src="https://www.youtube.com/embed/cFy1Fr1rDyE?si=YaRGY_mQEJKt_Gi9" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>


### Com crear escriptoris i plantilles

Aquest videotutorial et guiarà a través del procés de creació d'[escriptoris virtuals](https://isard.gitlab.io/isardvdi-docs/user/create_desktop.ca/) i [plantilles](https://isard.gitlab.io/isardvdi-docs/advanced/templates.ca/) en la plataforma d'IsardVDI, permetent-te organitzar i personalitzar el teu entorn de treball de manera eficient. Aprendràs a configurar escriptoris virtuals i a crear plantilles que simplificaran la creació de nous escriptoris, estalviant-te temps i esforç, així, optimitzant la teva productivitat.

<iframe width="560" height="315" src="https://www.youtube.com/embed/-nKtud9A_xo?si=ZHlo8G-bfx9ooSeY" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

### Com duplicar plantilles

En aquest videotutorial, aprendràs com [duplicar plantilles](https://isard.gitlab.io/isardvdi-docs/advanced/templates.ca/#duplicar-plantilla) en IsardVDI de manera ràpida i senzilla. La duplicació de plantilles és una habilitat útil que et permet crear noves configuracions basades en models prèviament establerts, estalviant-te temps i esforç. 

<iframe width="560" height="315" src="https://www.youtube.com/embed/FisGe3Unkew?si=tRHWAZGf4EydS2Gp" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

### Com crear un desplegament

Aquest videotutorial et guiarà a través del procés de creació de [desplegaments](https://isard.gitlab.io/isardvdi-docs/advanced/deployments.ca/) en la plataforma d'IsardVDI.

Amb els desplegaments, podeu crear i gestionar diversos escriptoris simultàniament per a tots els usuaris seleccionats. Com a creador, podeu gestionar fàcilment tots els escriptoris en un sol lloc a través de la secció Desplegaments. Fins i tot teniu la capacitat d'interactuar amb cada escriptori i veure les pantalles simultàniament.

<iframe width="560" height="315" src="https://www.youtube.com/embed/aaeNXC-Qyv0?si=-P6JzvKuRFTbDRYp" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>


## Vídeos curts

### Què és IsardVDI i què ofereix?


En aquest vídeo curt, t'introduirem a IsardVDI i et mostrarem el que aquesta potent plataforma té per a oferir. IsardVDI és una solució de virtualització d'escriptoris altament versàtil i eficient que brinda una sèrie de beneficis per a millorar el teu entorn informàtic.

Descobreix què és IsardVDI i com pot transformar la teva manera de treballar. Aprendràs sobre les seves característiques clau, que inclouen la creació d'escriptoris virtuals personalitzables, l'administració centralitzada, l'escalabilitat, i molt més.

<iframe width="560" height="315" src="https://www.youtube.com/embed/hm1TdKbWDCQ?si=D466Ycqfk0tMG3As" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>


### Tipus de visors

No tens molt clar els avantatges dels diferents tipus de [visors](https://isard.gitlab.io/isardvdi-docs/user/viewers/viewers.ca/) que hi ha a IsardVDI? Amb aquest vídeo curt, entendràs els avantatges d'utilitzar l'un o l'altre per a quan millor et convingui i depenent de l'ús que vagis a fer. 

<iframe width="560" height="315" src="https://www.youtube.com/embed/15aaRbkFUuk?si=CLpS0wzeNSoMKcBM" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

### Plantilles en acció

Com gaudir al màxim de la plataforma? Desplegaments amb les teves pròpies plantilles o les que proporciona IsardVDI. Es pot configurar qualsevol plantilla perquè sigui del teu agrado amb el programari que es necessiti i compartir-la amb els usuaris. A més, es pot crear un desplegament, on faràs una creació en massa d'escriptoris virtuals amb la teva plantilla als usuaris que escullis.

<iframe width="560" height="315" src="https://www.youtube.com/embed/6WktK3R7dTQ?si=HCuCjBt8zGPBCrIJ" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

### Més enllà d'un escriptori virtual

Què més es pot fer amb Isard? Amb aquest vídeo curt, explicarà altres possibilitats que pot cobrir la nostra plataforma, com per exemple les utilitzacions de targetes gpus, connectar dispositius usb, crear un entorn de pràctiques per formació de xarxes, entre altres.

<iframe width="560" height="315" src="https://www.youtube.com/embed/5QQBKTEl4zM?si=9lVT_brJ05XIKKTi" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>